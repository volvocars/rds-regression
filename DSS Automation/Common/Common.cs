﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using System;
using System.Collections.Generic;
using System.Linq;
using ExcelDataReader;
using System.Data;
using System.IO;
using OpenQA.Selenium.Support.UI;

namespace DSS_Automation
{
    [TestClass]
    public class Common
    {
        //string declaration
        public string previewURL;


        // To verify the text in End UI is matching with the provided text input
        [TestMethod]
        public void AssertIsTrueMethod(IWebDriver driver, IWebElement Element, string Verification)
        {
            string ContentText = Element.GetAttribute("innerHTML");
            Assert.IsTrue(ContentText.Contains(Verification));
        }

        //Scroll to the element in the page
        [TestMethod]
        public void ScrollToElementPosition(IWebElement Element, IWebDriver driver)
        {
            ((IJavaScriptExecutor)driver).ExecuteScript("arguments[0].scrollIntoView(true);", Element);
            Element.Click();
        }


        //Scroll to the element in the page
        [TestMethod]
        public void ScrollToElementIfNeeded(IWebElement Element, IWebDriver driver)
        {
            ((IJavaScriptExecutor)driver).ExecuteScript("arguments[0].scrollIntoViewIfNeeded(true);", Element);
            Element.Click();
        }
        //Scroll to bottom of the page
        public void scrollToBottom(IWebDriver driver)
        {
            ((IJavaScriptExecutor)driver).ExecuteScript("window.scrollTo(0,document.body.scrollHeight)");

        }



        //wait for element until the element is clickable
        public IWebElement waitForElementUntilClickable(string xPath, IWebDriver driver, int time)
        {
            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(time));
            wait.PollingInterval = TimeSpan.FromMilliseconds(250);
            IWebElement waitElement = wait.Until(ExpectedConditions.ElementToBeClickable(By.XPath(xPath)));
            return waitElement;
        }
        //wait for element until the element is visible
        public IWebElement waitForElementUntilVisible(string xPath, IWebDriver driver, int time)
        {
            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(time));
            wait.PollingInterval = TimeSpan.FromMilliseconds(250);
            IWebElement waitElement = wait.Until(ExpectedConditions.ElementIsVisible(By.XPath(xPath)));
            return waitElement;
        }
        //Wait Implicitly until the supplied timespan
        public void implicitWaitMethod(int time, IWebDriver driver)
        {
            //driver.Manage().Timeouts().ImplicitlyWait(TimeSpan.FromSeconds(time));
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(time);
        }

        //wait for element using fluent wait with xpath locator

        public IWebElement fluentWaitusingXpath(IWebDriver x, string xpath)
        {
            DefaultWait<IWebDriver> wait = new DefaultWait<IWebDriver>(x);
            wait.PollingInterval = TimeSpan.FromMilliseconds(250);
            wait.Message = "Can't find element";
            wait.Timeout = TimeSpan.FromSeconds(40);
            wait.IgnoreExceptionTypes(typeof(NoSuchElementException));

            IWebElement elem = wait.Until(driver => driver.FindElement(By.XPath(xpath)));
            return elem;
        }


        // Move to the element position based on the provided xpath using actions
        [TestMethod]
        public void MovetoElementMethod(IWebDriver driver, string Xpath)
        {
            IWebElement Element = driver.FindElement(By.XPath(Xpath));
            Actions act = new Actions(driver);
            act.MoveToElement(Element);
            act.Perform();
            Element.Click();
        }

        // Move to the element position based on the provided element value using actions
        [TestMethod]
        public void MovetoElementMethodFromElement(IWebDriver driver, IWebElement ElementValue)
        {
            Actions act = new Actions(driver);
            act.MoveToElement(ElementValue);
            act.Perform();
            ElementValue.Click();
        }

        // Move to the element position based on the provided element value using actions and does not click on the element
        [TestMethod]
        public void MovetoElementActionPerform(IWebDriver driver, IWebElement ElementValue)
        {
            Actions act = new Actions(driver);
            act.MoveToElement(ElementValue);
            act.Perform();
        }

        //}
        //    }
        //namespace DSS_Automation
        //{
        //    [TestClass]
        //    public class ExcelLib
        //    {

        [TestMethod]
        public static DataTable ExcelToDataTable(string fileName)
        {
            //open file and returns as Stream
            FileStream stream = File.Open(fileName, FileMode.Open, FileAccess.Read);
            //Createopenxmlreader via ExcelReaderFactory
            IExcelDataReader excelReader = ExcelReaderFactory.CreateOpenXmlReader(stream); //.xlsx
            //Set the First Row as Column Name
            //excelReader.IsFirstRowAsColumnNames = true;
            //Return as DataSet
            DataSet result = excelReader.AsDataSet();
            //Get all the Tables
            DataTableCollection table = result.Tables;
            //Store it in DataTable
            DataTable resultTable = table["Sheet1"];
            //return
            return resultTable;
            File.Delete(fileName);
        }
        static List<Datacollection> dataCol = new List<Datacollection>();

        [TestMethod]
        public static Tuple<int, int> PopulateInCollection(string fileName)
        {
            DataTable table = ExcelToDataTable(fileName);
            int RowCount = table.Rows.Count;
            int ColumnCount = table.Columns.Count;
            //Iterate through the rows and columns of the Table
            for (int row = 1; row <= table.Rows.Count; row++)
            {
                for (int col = 0; col < table.Columns.Count; col++)
                {
                    Datacollection dtTable = new Datacollection()
                    {
                        rowCount = table.Rows.Count,
                        rowNumber = row,
                        colName = table.Columns[col].ColumnName,
                        colValue = table.Rows[row - 1][col].ToString(),
                        colCount = table.Columns.Count
                    };
                    //Add all the details for each row
                    dataCol.Add(dtTable);
                }
            }
            return Tuple.Create(RowCount, ColumnCount);
        }

        [TestMethod]
        public int RowCountMethod()
        {
            try
            {
                int rcdata = (from rctdata in dataCol select rctdata.rowCount).First();
                return rcdata;
            }
            catch (Exception)
            {
                return 1;

            }

        }
        [TestMethod]
        public int CollCountMethod()
        {
            try
            {
                int cldata = (from cltdata in dataCol select cltdata.colCount).First();
                return cldata;
            }
            catch (Exception)
            {
                return 1;
            }

        }

        [TestMethod]
        public string ReadData(int rowNumber, string columnName)
        {
            try
            {

                //Retriving Data using LINQ to reduce much of iterations
                string data = (from colData in dataCol
                               where colData.colName == columnName && colData.rowNumber == rowNumber
                               select colData.colValue).SingleOrDefault();

                //var datas = dataCol.Where(x => x.colName == columnName && x.rowNumber == rowNumber).SingleOrDefault().colValue;
                return data.ToString();

            }
            catch (Exception)
            {
                return null;
            }
        }
        [TestMethod]
        public void ExcelCallMethod(IWebDriver driver, string filePath)
        {
            LoginClass loginobject = new LoginClass(driver);
            PopulateInCollection(filePath);
            int localRCTdata = RowCountMethod();
            int localColCTdata = CollCountMethod();
            for (int row = 1; row <= localRCTdata; row++)
            {
                string RoleValue = ReadData(row, "Column0");
                string UserName = ReadData(row, "Column1");
                string Password = ReadData(row, "Column2");


                if (RoleValue == "Showroom admin")
                {
                    loginobject.loginmethod(driver, UserName, Password);
                    //loginobject.showroomadminlogoutmethod(driver);
                }
                else if (RoleValue == "Access admin")
                {
                    loginobject.loginmethod(driver, UserName, Password);
                    //loginobject.AccessAdminLogoutMethod(driver);
                }
                else
                {
                }
            }

        }

    }
}

public class Datacollection
{
    public int rowNumber { get; set; }
    public string colName { get; set; }
    public string colValue { get; set; }
    public int rowCount { get; set; }
    public int colCount { get; set; }
}
