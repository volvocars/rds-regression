﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using System.Configuration;

namespace DSS_Automation
{
    [TestClass]
    public class LoginClass
    {

        public LoginClass(IWebDriver driver)
        {
            PageFactory.InitElements(driver, this);
        }
        //Account selection page
        [FindsBy(How = How.XPath, Using = "//span[contains(text(),'fsbiz.volvocars.biz')]")]
        public IWebElement accountSelectionXpath { get; set; }

        //Login page objects
        [FindsBy(How = How.Id, Using = "userNameInput")]
        public IWebElement uName { get; set; }

        [FindsBy(How = How.Id, Using = "passwordInput")]
        public IWebElement password { get; set; }

        [FindsBy(How = How.Id, Using = "submitButton")]
        public IWebElement loginButton { get; set; }


        //Showroom Admin logout
        //User Admin drop down
        [FindsBy(How = How.XPath, Using = "//*[@id='dname']")]
        public IWebElement userAdminPage { get; set; }

        //User Admin Logout page
        [FindsBy(How = How.XPath, Using = "//*[@id='volvo-dss']/div[1]/div/div[3]/div/ul[1]/li/a")]
        public IWebElement uaDropDown { get; set; }

        [FindsBy(How = How.XPath, Using = "//*[@id='volvo-dss']/div[1]/div/div[3]/div/ul[2]/li/a")]
        public IWebElement uaLogout { get; set; }

        //Showroom Home page
        [FindsBy(How = How.XPath, Using = "//*[@id='showroomHome']/div[1]/a")]
        public IWebElement showroomHomeLogoutCTA { get; set; }

        //Access admin drop down


        [FindsBy(How = How.XPath, Using = "//*[@id='volvo-dss']/div[1]/div/div[3]/div/ul[1]/li/a")]
        public IWebElement accessAdminDropDownCTA { get; set; }

        //Access admin Log out
        [FindsBy(How = How.XPath, Using = "//*[@id='volvo-dss']/div[1]/div/div[3]/div/ul[2]/li/a")]
        public IWebElement accessAdminLogoutCTA { get; set; }

        //variable declaration
        public string URL = ConfigurationManager.AppSettings["browseURL"];

        //Login method
        [TestMethod]
        public void loginmethod(IWebDriver driver, string UserNameValue, string PasswordValue)
        {
            driver.Navigate().GoToUrl(URL);
            driver.Manage().Window.Maximize();
            if (accountSelectionXpath != null)
            {
                accountSelectionXpath.Click();
            }
            System.Threading.Thread.Sleep(3000);
            uName.Clear();
            uName.SendKeys(UserNameValue);
            password.SendKeys(PasswordValue);
            loginButton.Click();
        }

        //Showroom admin log out
        public void showroomadminlogoutmethod(IWebDriver driver)
        {
            System.Threading.Thread.Sleep(3000);
            By by = By.XPath("//*[@id='dname']");
            var element = driver.FindElements(by).Count >= 1 ? driver.FindElement(by) : null;

            By ShowroomHomeby = By.XPath("//*[@id='showroomHome']/div[1]/a");
            var showroomhomeelement = driver.FindElements(ShowroomHomeby).Count >= 1 ? driver.FindElement(ShowroomHomeby) : null;

            if (element != null)
            {
                uaDropDown.Click();
                uaLogout.Click();
            }

            else if (showroomhomeelement != null)
            {
                showroomHomeLogoutCTA.Click();
            }
            else
            {
            }
        }

        //Access admin log out

        public void AccessAdminLogoutMethod(IWebDriver driver)
        {
            System.Threading.Thread.Sleep(3000);
            accessAdminDropDownCTA.Click();
            accessAdminLogoutCTA.Click();
        }


    }
}
