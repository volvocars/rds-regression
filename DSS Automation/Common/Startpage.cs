﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.PageObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DSS_Automation
{
    [TestClass]
    public class Startpage
    {

        public Startpage(IWebDriver driver)
        {
            PageFactory.InitElements(driver, this);
        }

        [FindsBy(How = How.XPath, Using = "//a[contains(@href,'/Studio\\Volvo Studios Milan')]")]
        public IWebElement selectLink { get; set; }

        [FindsBy(How = How.XPath, Using = "//h3[contains(@class,'admin-heading')]")]
        public IWebElement showroomTitle { get; set; }

        [FindsBy(How = How.XPath, Using = "//*[@id='optionsRadios1']")]
        public IWebElement contentEditorRole { get; set; }

        [FindsBy(How = How.XPath, Using = "//*[@id='optionsRadios2']")]
        public IWebElement accessAdminRole { get; set; }

        [FindsBy(How = How.ClassName, Using = "js-continue")]
        public IWebElement showroomHomeContinue { get; set; }

        [FindsBy(How = How.XPath, Using = "//*[@id='optionsRadios3']")]
        public IWebElement showroomAdminRole { get; set; }



        public void startpageMethod(IWebDriver driver, string userRole, string userName, string passWord)
        {
            LoginClass logincall = new LoginClass(driver);
            logincall.loginmethod(driver, userName, passWord);
            //ExcelLib ExcelObj = new ExcelLib();
            //ExcelObj.ExcelCallMethod(driver);                       
            System.Threading.Thread.Sleep(12000);
            // Verifying the showroom text
            //Commenting the 2 lines as the text is not getting captured.
            //string ShowroomtitleText = showroomTitle.GetAttribute("innerHTML");
            //Assert.IsTrue(ShowroomtitleText.Contains("Showrooms"));
            //selectlink.Click();
            string openedURL = driver.Url;
            if (openedURL.Contains("ShowroomHome"))
            {
                if (userRole == "Content Editor")
                {
                    contentEditorRole.Click();

                }
                else if (userRole == "Access Admin")
                {
                    accessAdminRole.Click();
                }
                else
                {
                    showroomAdminRole.Click();
                }
                showroomHomeContinue.Click();
            }
            else if (openedURL.Contains("AdminRolesLanding"))
            {
                if (userRole == "Content Editor")
                {
                    contentEditorRole.Click();

                }
                else if (userRole == "Access Admin")
                {
                    accessAdminRole.Click();
                }
                else
                {
                    showroomAdminRole.Click();
                }
                showroomHomeContinue.Click();
            }

            else { }

            System.Threading.Thread.Sleep(10000);
        }


    }
}
