﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Remote;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Reflection;
using excel = Microsoft.Office.Interop.Excel;


namespace DSS_Automation
{
    [TestClass]
    public class ShowroomPathGenerator
    {
        public string apendURL = ConfigurationManager.AppSettings["showroomCarApendURL"];
        public Startpage startPage_Object;
        public string supportedBrowser { get; set; }
        public string sessionName { get; set; }
        [TestInitialize]
        public void InitiateTest()
        {
            supportedBrowser = "Chrome_Browser";
            sessionName = "Edit All Showroom";
            SetUpBrowserStackDriver();
            startPage_Object = new Startpage(driver);
            // startPage_Object.startpageMethod(driver, "Showroom Admin", @"vccbiz\speriyas", "Selva!^volvo");
        }

    //    [DataSource("System.Data.Odbc", "Dsn=Excel Files;Driver={Microsoft Excel Driver (*.xls)};dbq=|DataDirectory|\\Test Data_VS.xlsx;defaultdir=.;driverid=790;maxbuffersize=2048;pagetimeout=5;readonly=true", "Sheet2$", DataAccessMethod.Sequential)]
        [TestMethod]
        [TestCategory("Showroom Car Path Generator")]
        public void showroomPathGeneratorCallMethod()
        {
            excel.Application showApplication = new excel.Application();
            var path = System.IO.Path.GetDirectoryName(Assembly.GetExecutingAssembly().GetName().CodeBase);
            path = path.Substring(6).Replace("\\bin\\Debug", "");
            excel.Workbook showWorkBook = showApplication.Workbooks.Open(path + @"\Test Data_VS.xlsx");
            excel.Worksheet showWorkSheet = showWorkBook.Sheets[2];
            excel.Range showRange = showWorkSheet.UsedRange;
            var rowCount = showWorkSheet.UsedRange.Rows.Count;
            var colCount = showWorkSheet.UsedRange.Columns.Count;
            // driver.FindElement(By.XPath("//*[@id='volvo - dss']/div[2]/div/div[2]/div[4]/div[3]/p/a")).Click();
            try
            {
                //var value1 = Convert.ToString(showWorkSheet.Cells[1, 1]);
                //var value2 = Convert.ToString(showRange.Cells[1][1]);
                //excel.Range rang = (excel.Range)showWorkSheet.Cells[1, 7];
                //var result = rang.Value.ToString();
                ((excel.Range)showWorkSheet.Cells[1, colCount + 1]).Value = "Showroom car Path";
                for (int rowIncrement = 2; rowIncrement <= rowCount; rowIncrement++)
                {
                    excel.Range carUpperSuffixValueRange = (excel.Range)showWorkSheet.Cells[rowIncrement, 7];
                    var carUpperSuffixValue = carUpperSuffixValueRange.Value.ToString();
                    excel.Range carLowerSuffixValueRange = (excel.Range)showWorkSheet.Cells[rowIncrement, 9];
                    var secondaryTitleEdit = carLowerSuffixValueRange.Value.ToString();
                    string PreviewXPath = "//p[contains(text(),'" + carUpperSuffixValue + " " + secondaryTitleEdit + "')]/parent::node()/following-sibling::div/p/a[contains(text(),'Preview')]";
                    string hrefValue = driver.FindElement(By.XPath(PreviewXPath)).GetAttribute("href");
                    string showroomCarURL = apendURL + hrefValue;
                    ((excel.Range)showWorkSheet.Cells[rowIncrement, colCount + 1]).Value = showroomCarURL;
                }
                showWorkBook.Save();
                showApplication.Quit();

            }
            catch (Exception)
            {
                showWorkBook.Close();
                showApplication.Quit();
            }
        }

        #region Browserstack
        // public string env = ConfigurationManager.AppSettings["Environment"];

        //Login for Sitecore for Authoring
        public const string usernameTextBox = ".//*[@id='UserName']";
        public const string passwordTextBox = ".//*[@id='Password']";
        public const string loginButton = ".//*[@id='login']/input";
        //BrowserStack credencials

        #region BrowserStack Configurations

        /* public string GetTestMethodName(string name)
        {
            return TestContext.TestName;

        }*/
        public string BrowserStackUserValue { get; set; }
        public string BrowserStackKey { get; set; }
        public string methodName;
        //  public TestContext TestContext { get; set; }
        //Web or Mobile version to Launch
        //public string supportedBrowser = ConfigurationManager.AppSettings["TargetBrowser"];
        public Dictionary<string, object> BrowserCapability { get; set; }

        public IWebDriver driver;

        //public Driver()
        //{
        //    BrowserStackUser = ConfigurationManager.AppSettings["BrowserStackUser"];
        //    BrowserStackKey = ConfigurationManager.AppSettings["BroserStackKey"];
        //    //Initiale all the properties of Browser that need to be launched
        //    InitialiseBrowser(supportedBrowser);
        //    //Initialise and declare desired capability
        //    DesiredCapabilities capability = new DesiredCapabilities(BrowserCapability);


        //    //Global driver to launch Browser stack with username and Accesskey
        //    driver = new RemoteWebDriver(
        //   new Uri("http://hub-cloud.browserstack.com/wd/hub/"), capability
        // );

        //}

        public void SetUpBrowserStackDriver()
        {
            BrowserStackUserValue = ConfigurationManager.AppSettings["BrowserStackUser"];
            BrowserStackKey = ConfigurationManager.AppSettings["BroserStackKey"];
            //Initiale all the properties of Browser that need to be launched
            InitialiseBrowser(supportedBrowser);
            //Initialise and declare desired capability
            DesiredCapabilities capability = new DesiredCapabilities(BrowserCapability);
            //Global driver to launch Browser stack with username and Accesskey
            driver = new RemoteWebDriver(
           new Uri("http://hub-cloud.browserstack.com/wd/hub/"), capability
         );

        }

        public void InitialiseBrowser(string supportedBrowser)
        {
            switch (supportedBrowser)
            {
                case "Chrome_Browser":
                    //Tests.HomePageTest.GetTestMethodName();
                    ChromeNavigatorOnBrowserStack();
                    break;

                case "Firefox_Browser":
                    FirefoxNavigator();
                    break;

                case "iPhone6":
                    iPhone6NavigatorOnBrowserStack();
                    break;
                case "iPadMini":
                    iPadMiniNavigatorOnBrowserStack();
                    break;
                case "Galaxy":
                    AndriodGalaxyTabOnBrowserStack();
                    break;

                default:
                    FirefoxNavigator();
                    break;
            }
        }

        private void FirefoxNavigator()
        {
            BrowserCapability = new Dictionary<string, object>() {
                        { "browserstack.user",     BrowserStackUserValue },
                        { "browserstack.key",      BrowserStackKey },
                        //{ "browserstack.local",    "true" },
                        { "browserstack.debug",    "true" },//for screenshots
                        { "browser",              "FireFox" },
                        { "browser_version",      "49.0" },
                        { "os",                   "Windows" },
                        { "os_version",           "10" },
                       // { "resolution",          "2048x1536" },
                        { "build",              "00001" },
                        { "project",            "DSSAutomationFramework" }
                    };

        }

        private void iPhone6NavigatorOnBrowserStack()
        {
            BrowserCapability = new Dictionary<string, object>() {
                        { "browserstack.user",     BrowserStackUserValue },
                        { "browserstack.key",      BrowserStackKey },
                        { "browserstack.local",    "false" },
                        { "browserstack.debug",    "true" },
                        {"platform", "MAC"},
                        { "device",             "iPhone 6S Plus" },
                        { "os",                   "iOS" },
                        { "build",             "Regression_Parallelthreadstrial" },
                        { "project",            "DSSAutomationFramework" },
                        {"name", "Iphone6" }
                       // { "realMobile", "true"}
                    };

        }

        private void ChromeNavigatorOnBrowserStack()
        {
            BrowserCapability = new Dictionary<string, object>() {
                        { "browserstack.user",     BrowserStackUserValue },
                        { "browserstack.key",      BrowserStackKey },
                        { "browserstack.local",    "False" },
                        { "browserstack.debug",    "true" },
                        { "browser",              "Chrome" },
                        { "browser_version",      "65.0" },
                        { "os",                   "Windows" },
                        { "os_version",           "10" },
                         {"quality",        "Compressed" },
                        { "build",              "Regression_DSS" },
                        { "project",            "DSSAutomationFramework" },
                         {"resolution","2048x1536"},
                         {"name",sessionName}

                    };
        }
        private void Chrome1NavigatorOnBrowserStack()
        {
            BrowserCapability = new Dictionary<string, object>() {
                        { "browserstack.user",     BrowserStackUserValue },
                        { "browserstack.key",      BrowserStackKey },
                        //{ "browserstack.local",    "true" },
                        { "browserstack.debug",    "true" },
                        { "browser",              "Chrome" },
                        { "browser_version",      "54.0" },
                        { "os",                   "Windows" },
                        { "os_version",           "8.1" },
                        {"quality",        "Compressed" },
                        { "build",              "Chrome" },
                        { "project",            "DSSAutomationFramework" },
                        {"resolution","1280x800"}
                    };
        }

        private void AndriodGalaxyTabOnBrowserStack()
        {
            BrowserCapability = new Dictionary<string, object>() {
                        { "browserstack.user",     BrowserStackUserValue },
                        { "browserstack.key",      BrowserStackKey },
                        { "browserstack.local",    "true" },
                        { "browserstack.debug",    "true" },
                        { "device",             "Samsung Galaxy S6" },
                        { "os",                   "Android" },
                        { "build",              "Demo_Galaxy" },
                        { "project",            "DSSAutomationFramework" },
                        {"name", "Demo_Home"},
                        {"deviceOrientation","portrait"},
                        {"realMobile","true"}
                    };

        }

        private void iPadMiniNavigatorOnBrowserStack()
        {
            BrowserCapability = new Dictionary<string, object>() {
                        { "browserstack.user",     BrowserStackUserValue },
                        { "browserstack.key",      BrowserStackKey },
                        { "browserstack.local",    "false" },
                        { "browserstack.debug",    "true" },
                        {"platform", "MAC"},
                        { "device",             "iPad Mini" },
                        { "os",                   "iOS" },
                        { "build",              "Mobile_iPhone" },
                        { "project",            "DSSAutomationFramework" },
                        {"name", "Iphone6SExecution"}
                       // { "realMobile",          "true"}
                    };

        }


        #endregion
        #endregion



    }
}
