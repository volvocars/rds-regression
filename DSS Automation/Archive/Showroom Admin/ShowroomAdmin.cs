﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Windows.Forms;

namespace DSS_Automation
{
    [TestClass]
    public class Sprint1_Object
    {
        public Common co_object;
        public Sprint1_Object(IWebDriver Wdriver)
        {
            PageFactory.InitElements(Wdriver, this);
            co_object = new Common();

        }

        //TS1
        [FindsBy(How = How.Id, Using = "DealerName")]
        public IWebElement showroomName { get; set; }

        [FindsBy(How = How.XPath, Using = "//input[contains(@class,'checkbox dealer-hide')]")]
        public IWebElement hideCheckBox { get; set; }


        //TS2

        [FindsBy(How = How.XPath, Using = "//button[contains(text(),'Create New Car')]")]
        public IWebElement createNewCar { get; set; }

        [FindsBy(How = How.Id, Using = "ModelTemplateSelectedId")]
        public IWebElement carModelTemplate { get; set; }

        [FindsBy(How = How.Id, Using = "ModelTemplateLanguageSelectedId")]
        public IWebElement marketDD { get; set; }

        [FindsBy(How = How.Id, Using = "ShowroomLanguages")]
        public IWebElement languageDDID { get; set; }

        [FindsBy(How = How.XPath, Using = "//button[contains(@class,'btn dropdown-toggle bs-placeholder btn-default')]")]
        public IWebElement languageDD { get; set; }

        [FindsBy(How = How.XPath, Using = "//a/span[contains(text(),'en-GB')]")]
        public IWebElement languageDDSelection { get; set; }

        [FindsBy(How = How.XPath, Using = "//div[contains(@class,'btn-group bootstrap-select show-tick selectshowroomLanguage dd-close')]/button/span[contains(text(),'en-GB')]")]
        public IWebElement languageDDEnter { get; set; }

        [FindsBy(How = How.Id, Using = "CarTitle")]
        public IWebElement carTitle { get; set; }

        [FindsBy(How = How.Id, Using = "CarUpperSuffixTitle")]
        public IWebElement carUpperSuffix { get; set; }

        [FindsBy(How = How.Id, Using = "CarLowerSuffixTitle")]
        public IWebElement carLowerSuffix { get; set; }

        [FindsBy(How = How.Id, Using = "CarSecondaryTitle")]
        public IWebElement carSecondaryTitle { get; set; }

        [FindsBy(How = How.Id, Using = "Price")]
        public IWebElement price { get; set; }

        [FindsBy(How = How.Id, Using = "StartingPrice")]
        public IWebElement startingPrice { get; set; }

        [FindsBy(How = How.Id, Using = "VINNumber")]
        public IWebElement vinNumber { get; set; }

        [FindsBy(How = How.XPath, Using = "//button[contains(text(),' Search')]")]
        public IWebElement searchButton { get; set; }

        [FindsBy(How = How.Id, Using = "SelectedModelyear")]
        public IWebElement modelYear { get; set; }

        [FindsBy(How = How.Id, Using = "ModelTemplateVariantSelectedId")]
        public IWebElement modelVariant { get; set; }

        [FindsBy(How = How.Id, Using = "KeyHighlightsTitle")]
        public IWebElement keyHighlightsTitle { get; set; }

        [FindsBy(How = How.Id, Using = "KeyHighlightsCollection_0__KeyHighlightsName")]
        public IWebElement keyHighlight1 { get; set; }

        [FindsBy(How = How.Id, Using = "KeyHighlightsCollection_1__KeyHighlightsName")]
        public IWebElement keyHighlight2 { get; set; }

        [FindsBy(How = How.XPath, Using = "//div/div/button[contains(text(),'Save')]")]
        public IWebElement save { get; set; }
                
        [FindsBy(How = How.XPath, Using = "//button[@class='button button-small js-dss-cancel']")]
        public IWebElement cancel { get; set; }

        [FindsBy(How = How.XPath, Using = "//button[@class='button-small button-opaque js-publish-all']")]
        public IWebElement publishAllCarsCTA { get; set; }

        //Navigate to the desired showroom page from showrooms list page
        public void showroomSelect(IWebDriver driver, string showroomNameValue)
        {
            try
            {
                string showroomSelectXpath = "//*[span[contains(text(),'" + showroomNameValue + "')]]/parent::node()/following-sibling::div/p/a[contains(text(),'Select')]";
                driver.FindElement(By.XPath(showroomSelectXpath)).Click();
            }
            catch (ElementNotVisibleException env)
            {
                var pageCount = driver.FindElements(By.XPath("//*[@id='page-nav']/ul/li")).Count();
                var nxtCount = pageCount - 1;
                for (var loopCount = 1; loopCount < nxtCount; loopCount++)
                {
                    try
                    {
                        driver.FindElement(By.XPath("//*[@id='page-nav']/ul/li/following-sibling::li/a[contains(text(),'Next')]")).Click();
                        string showroomSelectXpath = "//*[span[contains(text(),'" + showroomNameValue + "')]]/parent::node()/following-sibling::div/p/a[contains(text(),'Select')]";
                        driver.FindElement(By.XPath(showroomSelectXpath)).Click();
                    }
                    catch (ElementNotVisibleException envexcep)
                    {
                        continue;
                    }
                    break;
                }

            }

        }
        
        //Create New showroom car
        [TestMethod]
        public void createNewCarMethod(string carModelTemplateValue, string MarketLanguageDDValue, string languageDDLValue,string carTitleValue, string carUpperSuffixValue, string carLowerSuffixValue, string carSecondaryTitleValue, string PriceValue, string StartingAtValue, string VINNumberValue, string keyHighlightsTitleValue, string keyhighlightsdescValue, string showroomNameValue, IWebDriver driver)
        {
            System.Threading.Thread.Sleep(15000);
            ((IJavaScriptExecutor)driver).ExecuteScript("window.scrollTo(0,document.body.scrollHeight)");
            createNewCar.Click();

            System.Threading.Thread.Sleep(5000);

            //TS1
            showroomName.Clear();
            showroomName.SendKeys(showroomNameValue);
            hideCheckBox.Click();
            //TS2
            SelectElement MarketDDL = new SelectElement(marketDD);
            MarketDDL.SelectByText(MarketLanguageDDValue);

            System.Threading.Thread.Sleep(3000);

            //languageDD.SendKeys("Enter");
            //languageDDSelection.Click();
            //languageDDEnter.Click();

            SelectElement LanguageDDL = new SelectElement(languageDDID);
            LanguageDDL.SelectByText(languageDDLValue);

            vinNumber.SendKeys(VINNumberValue);
            searchButton.Click();
            System.Threading.Thread.Sleep(20000);

            //IWebElement loadIcon = driver.FindElement(By.XPath("//*[@id='volvo-dss']/div[2]/div[9]"));
            //bool loadIconVisible = loadIcon.Displayed;

            //if (loadIconVisible)
            //{
            //    WebDriverWait waitTime = new WebDriverWait(driver, TimeSpan.FromSeconds(150));
            //    waitTime.Until(wdriver => wdriver.FindElement(By.XPath("//*[@id='adminFormID']/div[2]/div[2]/div/div[1]/p")).Text.Contains("VIN Lookup"));
            //}
            SelectElement ModelTemplateDDL = new SelectElement(carModelTemplate);
            string ModelTemplateDDLSelectedOption = ModelTemplateDDL.SelectedOption.Text;
            if (ModelTemplateDDLSelectedOption == "")
            {
                ModelTemplateDDL.SelectByText(carModelTemplateValue);
            }

            SelectElement ModelYearDDL = new SelectElement(modelYear);
            var ModelYearDDLCount = ModelYearDDL.Options.Count;
            string ModelYearDDLSelectedOption = ModelYearDDL.SelectedOption.Text;
            if (ModelYearDDLSelectedOption == "")
            {
                if (ModelYearDDLCount != 0)
                {
                    ModelYearDDL.SelectByIndex(1);

                }

            }

            SelectElement ModelVariantDDL = new SelectElement(modelVariant);
            int ModelVariantDDlCount = ModelVariantDDL.Options.Count;
            if (ModelVariantDDlCount != 0)
            {
                ModelVariantDDL.SelectByIndex(1);
            }

            carTitle.SendKeys(carTitleValue);
            carUpperSuffix.SendKeys(carUpperSuffixValue);
            carLowerSuffix.SendKeys(carLowerSuffixValue);
            carSecondaryTitle.SendKeys(carSecondaryTitleValue);
            price.SendKeys(PriceValue);
            startingPrice.SendKeys(StartingAtValue);
            keyHighlightsTitle.SendKeys(keyHighlightsTitleValue);
            keyHighlight1.SendKeys(keyhighlightsdescValue);
            ((IJavaScriptExecutor)driver).ExecuteScript("window.scrollTo(0,document.body.scrollHeight)");
            save.Click();
        }

        //Edit the secondary title and save the changes.
        [TestMethod]
        public void editShowroomCar(IWebDriver driver, string USuffix, string Stitle, string Sectitle)
        {
            System.Threading.Thread.Sleep(8000);
            //((IJavaScriptExecutor)driver).ExecuteScript("window.scrollTo(0,document.body.scrollHeight)");
            string EditXpath = "//p[contains(text(),'" + USuffix + " " + Stitle + "')]/parent::node()/following-sibling::div/p/a[contains(text(),'Edit')]";
            co_object.MovetoElementMethod(driver, EditXpath);
            System.Threading.Thread.Sleep(3000);
            carSecondaryTitle.Clear();
            carSecondaryTitle.SendKeys(Sectitle);
            ((IJavaScriptExecutor)driver).ExecuteScript("window.scrollTo(0,document.body.scrollHeight)");
            save.Click();
        }

        //Edit the car and upload an image
        [TestMethod]
        public void imageUploadMethod(IWebDriver driver, string USuffix, string Stitle)
        {
            System.Threading.Thread.Sleep(8000);
            string EditXpath = "//p[contains(text(),'" + USuffix + " " + Stitle + "')]/parent::node()/following-sibling::div/p/a[contains(text(),'Edit')]";
            co_object.MovetoElementMethod(driver, EditXpath);
            System.Threading.Thread.Sleep(3000);
            driver.FindElement(By.XPath("//*[@id='adminFormID']/div[2]/div[10]/div/div[1]/span[1]/button")).Click();
            var path = System.IO.Path.GetDirectoryName(Assembly.GetExecutingAssembly().GetName().CodeBase);
            path = path.Substring(6).Replace("\\bin\\Debug", "");
            var fullPath = path + "\\Car-Wallpaper-On-Wallpaper-Hd-6-2732x2048.jpg";
            SendKeys.SendWait(fullPath);
            SendKeys.SendWait(@"{Enter}");
            driver.FindElement(By.XPath("//*[@id='adminFormID']/div[2]/div[10]/div/div[1]/button")).Click();
            System.Threading.Thread.Sleep(10000);
            save.Click();
        }

        [TestMethod]
        [TestCategory("Showroom Admin[Functional Validation]")]
        public void editAllShowroomCarCallMethod(IWebDriver driver)
        {
            int showroomCount = driver.FindElements(By.XPath("//div[@class='paginationWrapper custom-row']//div[@class='row list-row']//p/a")).Count();
            for (int scIncrement = 25; scIncrement <= showroomCount; scIncrement++)
            {
                try
                {
                    driver.FindElement(By.XPath("//div[@class='paginationWrapper custom-row']//div[@class='row list-row'][" + scIncrement + "]//p/a")).Click();
                    System.Threading.Thread.Sleep(5000);
                    int showroomCarCount = driver.FindElements(By.XPath("//div[@class='cars-list']//div[@class='paginationWrapper custom-row']//div[@class='row list-row']//p/a[contains(text(),'Edit')]")).Count();

                    for (int sCarIncrement = 1; sCarIncrement <= showroomCarCount; sCarIncrement++)
                    {
                        string editXPath = "//div[@class='cars-list']//div[@class='paginationWrapper custom-row']//div[@class='row list-row'][" + sCarIncrement + "]//p/a[contains(text(),'Edit')]";
                        co_object.MovetoElementMethod(driver, editXPath);
                        System.Threading.Thread.Sleep(12000);
                        searchButton.Click();
                        System.Threading.Thread.Sleep(5000);
                        string vinInputValue=vinNumber.GetAttribute("value");
                        if (vinInputValue != "")
                        {
                            IWebElement surePopup = driver.FindElement(By.XPath("/html/body/div[2]/div/div/div[2]/button[1]"));
                            if (surePopup.Displayed)
                            {
                                driver.FindElement(By.XPath("/html/body/div[2]/div/div/div[2]/button[1]")).Click();
                            }

                        }                                          
                       
                        System.Threading.Thread.Sleep(33000);
                        IWebElement casErrorCode = driver.FindElement(By.XPath("//input[@id='CasErrorCode']"));
                        string errorCodeValue = casErrorCode.GetAttribute("value");
                        if (errorCodeValue == "500" || errorCodeValue == "200")
                        {
                            cancel.Click();
                            System.Threading.Thread.Sleep(20000);
                        }
                        else
                        {
                            ((IJavaScriptExecutor)driver).ExecuteScript("window.scrollTo(0,document.body.scrollHeight)");
                            save.Click();
                            System.Threading.Thread.Sleep(40000);
                            string publishStatus = driver.FindElement(By.XPath("//div[@class='cars-list']//div[@class='paginationWrapper custom-row']//div[@class='row list-row'][" + sCarIncrement + "]/div[7]")).GetAttribute("innerText");
                            if (publishStatus.Contains("Publish"))
                                    {
                                driver.FindElement(By.XPath("//div[@class='cars-list']//div[@class='paginationWrapper custom-row']//div[@class='row list-row'][" + sCarIncrement + "]/div[7]")).Click();
                            }
                        }

                        System.Threading.Thread.Sleep(16000);
                    }
                    driver.FindElement(By.XPath("//a[text()='Showroom Admin']")).Click();
                    driver.FindElement(By.XPath("//a[text()='Showrooms']")).Click();
                }
                catch (ElementNotVisibleException env)
                {
                    var pageCount = driver.FindElements(By.XPath("//*[@id='page-nav']/ul/li")).Count();
                    var nxtCount = pageCount - 1;
                    for (var loopCount = 1; loopCount < nxtCount; loopCount++)
                    {
                        try
                        {
                            driver.FindElement(By.XPath("//*[@id='page-nav']/ul/li/following-sibling::li/a[contains(text(),'Next')]")).Click();
                            string showroomSelectXpath = "//div[@class='paginationWrapper custom-row']//div[@class='row list-row'][" + scIncrement + "]//p/a";
                            driver.FindElement(By.XPath(showroomSelectXpath)).Click();
                            System.Threading.Thread.Sleep(5000);
                            int showroomCarCount = driver.FindElements(By.XPath("//div[@class='cars-list']//div[@class='paginationWrapper custom-row']//div[@class='row list-row']//p/a[contains(text(),'Edit')]")).Count();
                            for (int sCarIncrement = 1; sCarIncrement <= showroomCarCount; sCarIncrement++)
                            {
                                string editXPath = "//div[@class='cars-list']//div[@class='paginationWrapper custom-row']//div[@class='row list-row'][" + sCarIncrement + "]//p/a[contains(text(),'Edit')]";
                                co_object.MovetoElementMethod(driver, editXPath); System.Threading.Thread.Sleep(12000);
                                searchButton.Click();
                                System.Threading.Thread.Sleep(1000);
                                string vinInputValue = vinNumber.GetAttribute("value");
                                if (vinInputValue != "")
                                {
                                    IWebElement surePopup = driver.FindElement(By.XPath("/html/body/div[2]/div/div/div[2]/button[1]"));
                                    if (surePopup.Displayed)
                                    {
                                        driver.FindElement(By.XPath("/html/body/div[2]/div/div/div[2]/button[1]")).Click();
                                    }

                                }
                                System.Threading.Thread.Sleep(15000);
                                IWebElement casErrorCode = driver.FindElement(By.XPath("//input[@id='CasErrorCode']"));
                                string errorCodeValue = casErrorCode.GetAttribute("value");
                                if (errorCodeValue == "500" || errorCodeValue == "200")
                                {
                                    cancel.Click();
                                    System.Threading.Thread.Sleep(20000);
                                }
                                else
                                {
                                    ((IJavaScriptExecutor)driver).ExecuteScript("window.scrollTo(0,document.body.scrollHeight)");
                                    save.Click();
                                    System.Threading.Thread.Sleep(50000);
                                    string publishStatus=driver.FindElement(By.XPath("//div[@class='cars-list']//div[@class='paginationWrapper custom-row']//div[@class='row list-row'][" + sCarIncrement + "]/div[7]")).GetAttribute("innerText");
                                    if (publishStatus.Contains("Publish"))
                                    {
                                        driver.FindElement(By.XPath("//div[@class='cars-list']//div[@class='paginationWrapper custom-row']//div[@class='row list-row'][" + sCarIncrement + "]/div[7]")).Click();
                                    }

                                 }

                                System.Threading.Thread.Sleep(16000);
                            }
                            driver.FindElement(By.XPath("//a[text()='Showroom Admin']")).Click();
                            driver.FindElement(By.XPath("//a[text()='Showrooms']")).Click();

                        }
                        catch (ElementNotVisibleException envexcep)
                        {
                            continue;
                        }
                        break;
                    }
                    System.Threading.Thread.Sleep(3000);
                }
            }
        }

        //Publish the edited showroom car from Sprint1TS3Method
        [TestMethod]
        public void publishCar(IWebDriver driver, string USuffix, string Sectitle)
        {
            System.Threading.Thread.Sleep(3000);
            string PublishXPath = "//p[contains(text(),'" + USuffix + " " + Sectitle + "')]/parent::node()/following-sibling::div/p/a[contains(text(),'Publish')]";
            co_object.MovetoElementMethod(driver, PublishXPath);
        }



        //preview the pubished car
        [TestMethod]
        public string showroomPathGenerator(IWebDriver driver, string USuffix, string Sectitle)
        {
            System.Threading.Thread.Sleep(10000);
            string PreviewXPath = "//p[contains(text(),'" + USuffix + " " + Sectitle + "')]/parent::node()/following-sibling::div/p/a[contains(text(),'Preview')]";
            System.Threading.Thread.Sleep(10000);
            co_object.MovetoElementMethod(driver, PreviewXPath);
            System.Threading.Thread.Sleep(30000);
            string tabName = driver.WindowHandles.Last();
            driver.SwitchTo().Window(tabName);
            System.Threading.Thread.Sleep(5000);
            return PreviewXPath;
        }

    }

}
