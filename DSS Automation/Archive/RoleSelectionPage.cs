﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;

namespace DSS_Automation
{
    [TestClass]
    public class RoleSelectionPage
    {
        public RoleSelectionPage(IWebDriver driver)
        {
            PageFactory.InitElements(driver, this);            
        }
        
        //Role Selection Page objects
        [FindsBy(How = How.XPath, Using = "//div[contains(text(),'Showroom Admin ')]")]        
        public IWebElement ShowroomAdminXpath { get; set; }

    }
}
