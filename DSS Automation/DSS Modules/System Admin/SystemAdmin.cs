﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;

namespace DSS_Automation.DSS_Modules.System_Admin
{
     [TestClass]
    public class SystemAdmin
    {

         public SystemAdmin(IWebDriver driver)
         {
             PageFactory.InitElements(driver, this);
         }

         //System Admin page objects

         [FindsBy(How = How.XPath, Using = "//*[contains(@class,'js-logo')]")]
         public IWebElement logo { get; set; }

         [FindsBy(How = How.XPath, Using = "//*[@id='sa-roleForm']/div/div[2]/div/button")]
         public IWebElement createNewUserRole { get; set; }

         [FindsBy(How = How.XPath, Using = "//*[@id='CDSID']")]
         public IWebElement CDSIDInput { get; set; }

         [FindsBy(How = How.XPath, Using = "//*[contains(@class,'selectRole dd-close')]")]
         public IWebElement assignedRole { get; set; }

         [FindsBy(How = How.XPath, Using = "//div[contains(@class,'custom-row saMarketEditor')]//button[contains(text(),'ADD MARKET')]")]
         public IWebElement addMarketCTAMarketEditor { get; set; }
         
         [FindsBy(How = How.XPath, Using = "//div[contains(@class,'custom-row samarketlan')]//button[contains(text(),'ADD MARKET')]")]
         public IWebElement addMarketCTAAccessAdmin { get; set; }

         [FindsBy(How = How.XPath, Using = "//button[contains(text(),'ADD LANGUAGE')]")]
         public IWebElement addLanguageCTA { get; set; }

         [FindsBy(How = How.XPath, Using = "//*[@id='AddUserRole']/div/div/div[2]/div[5]/div/button[1]")]
         public IWebElement accessAdminSave { get; set; }

         [FindsBy(How = How.XPath, Using = "//*[@id='AddUserRole']/div/div/div[2]/div[5]/div/button[1]")]
         public IWebElement marketEditorSave { get; set; }

         [FindsBy(How = How.XPath, Using = "//div[contains(@id,'addMarketModalEditor')]//button[contains(@class,'button-small button-opaque addMarket')]")]
         public IWebElement marketEditorOK { get; set; }

         [FindsBy(How = How.XPath, Using = "//div[contains(@class,'custom-row samarketlan')]//button[contains(@class,'button-small button-opaque addMarket')]")]
         public IWebElement aaAddMarketOK { get; set; }

         [FindsBy(How = How.XPath, Using = "//div[contains(@id,'addLanguage')]//button[contains(@class,'button-small button-opaque addLanguage')]")]
         public IWebElement aaAddLanguageOK { get; set; }

         [FindsBy(How = How.XPath, Using = "//a[contains(text(),'Log out')]")]
         public IWebElement saLogOutCTA { get; set; }

         [FindsBy(How = How.XPath, Using = "//a[contains(text(),'System Admin')]")]
         public IWebElement systemAdminMenu { get; set; }



        //market editor language //div[contains(@class,'custom-row saMarketEditor')]//div[contains(@class,'itemText')][contains(text(),'en-GB')]
        //Access Admin market  //div[contains(@class,'custom-row samarketlan')]//div[contains(@class,'row custom-row marketCheck')]//div[contains(@class,'itemText')][contains(text(),'en-GB')]
        //AccessAdmin language  //div[contains(@class,'custom-row samarketlan')]//div[contains(@class,'row custom-row langCheck')]//div[contains(@class,'itemText')][contains(text(),'en-GB')]
        [TestMethod]
        public void contentEditorRoleCreation(IWebDriver driver, string CDSID, string marketValue1)
        {
            System.Threading.Thread.Sleep(1000);
            createNewUserRole.Click();
            System.Threading.Thread.Sleep(1000);
            CDSIDInput.SendKeys(CDSID);
            SelectElement roleDD = new SelectElement(assignedRole);
            roleDD.SelectByText("Content Editor");
            addMarketCTAMarketEditor.Click();
            System.Threading.Thread.Sleep(200);
            string[] marketList = marketValue1.Split(',');
            for (int marketListIterator = 0; marketListIterator < marketList.Length; marketListIterator++)
            {
                marketList[marketListIterator] = marketList[marketListIterator].Trim();
                driver.FindElement(By.XPath("//div[contains(@class,'custom-row saMarketEditor')]//div[contains(@class,'itemText')][contains(text(),'" + marketList[marketListIterator] + "')]/preceding-sibling::input")).Click();

            }
            //driver.FindElement(By.XPath("//div[contains(@class,'custom-row saMarketEditor')]//div[contains(@class,'itemText')][contains(text(),'" + marketValue2 + "')]/preceding-sibling::input")).Click();
            marketEditorOK.Click();
            System.Threading.Thread.Sleep(200);
            marketEditorSave.Click();

        }

        [TestMethod]
        public void accessAdminRoleCreation(IWebDriver driver, string CDSID, string marketValue1, string languageValue1)
        {
            System.Threading.Thread.Sleep(1000);
            createNewUserRole.Click();
            System.Threading.Thread.Sleep(1000);
            CDSIDInput.SendKeys(CDSID);
            SelectElement roleDD = new SelectElement(assignedRole);
            roleDD.SelectByText("Access Admin");
            addMarketCTAAccessAdmin.Click();
            System.Threading.Thread.Sleep(200);
            string[] marketList = marketValue1.Split(',');
            for (int marketListIterator = 0; marketListIterator < marketList.Length; marketListIterator++)
            {
                marketList[marketListIterator] = marketList[marketListIterator].Trim();
                driver.FindElement(By.XPath("//div[contains(@class,'custom-row samarketlan')]//div[contains(@class,'row custom-row marketCheck')]//div[contains(@class,'itemText')][contains(text(),'" + marketList[marketListIterator] + "')]/preceding-sibling::input")).Click();
            }

            //driver.FindElement(By.XPath("//div[contains(@class,'custom-row samarketlan')]//div[contains(@class,'row custom-row marketCheck')]//div[contains(@class,'itemText')][contains(text(),'" + marketValue2 + "')]/preceding-sibling::input")).Click();
            aaAddMarketOK.Click();
            System.Threading.Thread.Sleep(100);
            addLanguageCTA.Click();
            System.Threading.Thread.Sleep(100);
            string[] languageList = languageValue1.Split(',');
            for (int languageValueIterator = 0; languageValueIterator < languageList.Length; languageValueIterator++)
            {
                languageList[languageValueIterator] = languageList[languageValueIterator].Trim();
                driver.FindElement(By.XPath("//div[contains(@class,'custom-row samarketlan')]//div[contains(@class,'row custom-row langCheck')]//div[contains(@class,'itemText')][contains(text(),'" + languageList[languageValueIterator] + "')]/preceding-sibling::input")).Click();
            }
            // driver.FindElement(By.XPath("//div[contains(@class,'custom-row samarketlan')]//div[contains(@class,'row custom-row langCheck')]//div[contains(@class,'itemText')][contains(text(),'" + languageValue2 + "')]/preceding-sibling::input")).Click();
            aaAddLanguageOK.Click();
            System.Threading.Thread.Sleep(100);
            accessAdminSave.Click();
        }
        [TestMethod]
         public void saLogoutMethod()
         {
             systemAdminMenu.Click();
             saLogOutCTA.Click();
         }
    }
}
