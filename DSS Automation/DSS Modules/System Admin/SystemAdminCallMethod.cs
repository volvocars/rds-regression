﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium.Remote;
using System.Configuration;
using OpenQA.Selenium;

namespace DSS_Automation.DSS_Modules.System_Admin
{
    [TestClass]
    public class SystemAdminCallMethod
    {
        public SystemAdmin sa_Object;
        public LoginClass logInObject;
        public string supportedBrowser;
        public string sessionName;
        public static string inputFilePath { get; set; }


        [TestInitialize]
        public void InitiateTest()
        {
            supportedBrowser = Convert.ToString(TestContext.DataRow["Browser Name"]);
            sessionName = Convert.ToString(TestContext.DataRow["Session Name"]);
            string userName = Convert.ToString(TestContext.DataRow["System Admin Username"]);
            string passWord = Convert.ToString(TestContext.DataRow["System Admin Password"]);
            //Browserstack S2 object creation
            SetUpBrowserStackDriver();
            sa_Object=new SystemAdmin(driver);
            logInObject=new LoginClass(driver);
            logInObject.loginmethod(driver,userName,passWord);
        }
        #region systemAdminCallMethod
        private TestContext testContextInstance;
        public TestContext TestContext
        {
            get { return testContextInstance; }
            set { testContextInstance = value; }
        }

        //To create a new user role in system admin page
        [DataSource("System.Data.Odbc", "Dsn=Excel Files;Driver={Microsoft Excel Driver (*.xls)};dbq=|DataDirectory|//Test Data_VS.xlsx;defaultdir=.;driverid=790;maxbuffersize=2048;pagetimeout=5;readonly=true", "Sheet3$", DataAccessMethod.Sequential)]
        [TestMethod]
        [TestCategory("System Admin[Functional Validation]")]
        public void createNewuserRole()
        {
            string selectedRole = Convert.ToString(TestContext.DataRow["Desired Role"]);
            string mappedCDSID = Convert.ToString(TestContext.DataRow["CDSID"]);
            string market1Value = Convert.ToString(TestContext.DataRow["Market"]);
            string language1Value = Convert.ToString(TestContext.DataRow["Language"]);
            if (selectedRole == "Content Editor")
            {
                sa_Object.contentEditorRoleCreation(driver, mappedCDSID, market1Value);
            }
            else
            {
                sa_Object.accessAdminRoleCreation(driver, mappedCDSID, market1Value, language1Value);
            }
        }

        //To log out from system admin page
        [DataSource("System.Data.Odbc", "Dsn=Excel Files;Driver={Microsoft Excel Driver (*.xls)};dbq=|DataDirectory|\\Test Data_VS.xlsx;defaultdir=.;driverid=790;maxbuffersize=2048;pagetimeout=5;readonly=true", "Sheet4$", DataAccessMethod.Sequential)]
        [TestMethod]
        [TestCategory("System Admin[Functional Validation]")]
        public void systemAdminLogOut()
        {
            sa_Object.saLogoutMethod();
        }

        [TestCleanup]
        public void accessAdminCleanup()
        {
            driver.Quit();
        }
        #endregion

        #region Browserstack
        // public string env = ConfigurationManager.AppSettings["Environment"];

        //Login for Sitecore for Authoring
        public const string usernameTextBox = ".//*[@id='UserName']";
        public const string passwordTextBox = ".//*[@id='Password']";
        public const string loginButton = ".//*[@id='login']/input";
        //BrowserStack credencials

        #region BrowserStack Configurations

        /* public string GetTestMethodName(string name)
        {
            return TestContext.TestName;

        }*/
        public string BrowserStackUserValue { get; set; }
        public string BrowserStackKey { get; set; }
        public string methodName;
        //  public TestContext TestContext { get; set; }
        //Web or Mobile version to Launch
        //public string supportedBrowser = ConfigurationManager.AppSettings["TargetBrowser"];
        public Dictionary<string, object> BrowserCapability { get; set; }

        public IWebDriver driver;

        //public Driver()
        //{
        //    BrowserStackUser = ConfigurationManager.AppSettings["BrowserStackUser"];
        //    BrowserStackKey = ConfigurationManager.AppSettings["BroserStackKey"];
        //    //Initiale all the properties of Browser that need to be launched
        //    InitialiseBrowser(supportedBrowser);
        //    //Initialise and declare desired capability
        //    DesiredCapabilities capability = new DesiredCapabilities(BrowserCapability);


        //    //Global driver to launch Browser stack with username and Accesskey
        //    driver = new RemoteWebDriver(
        //   new Uri("http://hub-cloud.browserstack.com/wd/hub/"), capability
        // );

        //}

        public void SetUpBrowserStackDriver()
        {
            BrowserStackUserValue = ConfigurationManager.AppSettings["BrowserStackUser"];
            BrowserStackKey = ConfigurationManager.AppSettings["BroserStackKey"];
            //Initiale all the properties of Browser that need to be launched
            InitialiseBrowser(supportedBrowser);
            //Initialise and declare desired capability
            DesiredCapabilities capability = new DesiredCapabilities(BrowserCapability);


            //Global driver to launch Browser stack with username and Accesskey
            driver = new RemoteWebDriver(
           new Uri("http://hub-cloud.browserstack.com/wd/hub/"), capability
         );

        }

        public void InitialiseBrowser(string supportedBrowser)
        {
            //BrowserStackUserValue = ConfigurationManager.AppSettings["BrowserStackUser"];
            //BrowserStackKey = ConfigurationManager.AppSettings["BroserStackKey"];
            //DesiredCapabilities capability = new DesiredCapabilities(BrowserCapability);
            //driver = new RemoteWebDriver(
            //    new Uri("http://hub-cloud.browserstack.com/wd/hub/"), capability
            //    );
            switch (supportedBrowser)
            {
                case "Chrome_Browser":
                    //Tests.HomePageTest.GetTestMethodName();
                    ChromeNavigatorOnBrowserStack();
                    break;

                case "Firefox_Browser":
                    FirefoxNavigator();
                    break;

                case "iPhone6":
                    iPhone6NavigatorOnBrowserStack();
                    break;
                case "iPadMini":
                    iPadMiniNavigatorOnBrowserStack();
                    break;
                case "Galaxy":
                    AndriodGalaxyTabOnBrowserStack();
                    break;

                default:
                    FirefoxNavigator();
                    break;
            }
        }

        private void FirefoxNavigator()
        {
            BrowserCapability = new Dictionary<string, object>() {
                        { "browserstack.user",     BrowserStackUserValue },
                        { "browserstack.key",      BrowserStackKey },
                        //{ "browserstack.local",    "true" },
                        { "browserstack.debug",    "true" },//for screenshots
                        { "browser",              "FireFox" },
                        { "browser_version",      "49.0" },
                        { "os",                   "Windows" },
                        { "os_version",           "10" },
                       // { "resolution",          "2048x1536" },
                        { "build",              "00001" },
                        { "project",            "DSSAutomationFramework" }
                    };

        }

        private void iPhone6NavigatorOnBrowserStack()
        {
            BrowserCapability = new Dictionary<string, object>() {
                        { "browserstack.user",     BrowserStackUserValue },
                        { "browserstack.key",      BrowserStackKey },
                        { "browserstack.local",    "false" },
                        { "browserstack.debug",    "true" },
                        {"platform", "MAC"},
                        { "device",             "iPhone 6S Plus" },
                        { "os",                   "iOS" },
                        { "build",             "Regression_Parallelthreadstrial" },
                        { "project",            "DSSAutomationFramework" },
                        {"name", "Iphone6" }
                       // { "realMobile", "true"}
                    };

        }

        private void ChromeNavigatorOnBrowserStack()
        {
            BrowserCapability = new Dictionary<string, object>() {
                        { "browserstack.user",     BrowserStackUserValue },
                        { "browserstack.key",      BrowserStackKey },
                        { "browserstack.local",    "False" },
                        { "browserstack.debug",    "true" },
                        { "browser",              "Chrome" },
                        { "browser_version",      "54.0" },
                        { "os",                   "Windows" },
                        { "os_version",           "10" },
                        {"quality",        "Compressed" },
                        { "build",              "Regression_DSS" },
                        { "project",            "DSSAutomationFramework" },
                        {"resolution","1920x1080"},
                        {"name",sessionName }

                    };
        }
        private void Chrome1NavigatorOnBrowserStack()
        {
            BrowserCapability = new Dictionary<string, object>() {
                        { "browserstack.user",     BrowserStackUserValue },
                        { "browserstack.key",      BrowserStackKey },
                        //{ "browserstack.local",    "true" },
                        { "browserstack.debug",    "true" },
                        { "browser",              "Chrome" },
                        { "browser_version",      "54.0" },
                        { "os",                   "Windows" },
                        { "os_version",           "8.1" },
                        {"quality",        "Compressed" },
                        { "build",              "Chrome" },
                        { "project",            "DSSAutomationFramework" },
                        {"resolution","1280x800"}
                    };
        }

        private void AndriodGalaxyTabOnBrowserStack()
        {
            BrowserCapability = new Dictionary<string, object>() {
                        { "browserstack.user",     BrowserStackUserValue },
                        { "browserstack.key",      BrowserStackKey },
                        { "browserstack.local",    "true" },
                        { "browserstack.debug",    "true" },
                        { "device",             "Samsung Galaxy S6" },
                        { "os",                   "Android" },
                        { "build",              "Demo_Galaxy" },
                        { "project",            "DSSAutomationFramework" },
                        {"name", "Demo_Home"},
                        {"deviceOrientation","portrait"},
                        {"realMobile","true"}
                    };

        }

        private void iPadMiniNavigatorOnBrowserStack()
        {
            BrowserCapability = new Dictionary<string, object>() {
                        { "browserstack.user",     BrowserStackUserValue },
                        { "browserstack.key",      BrowserStackKey },
                        { "browserstack.local",    "false" },
                        { "browserstack.debug",    "true" },
                        {"platform", "MAC"},
                        { "device",             "iPad Mini" },
                        { "os",                   "iOS" },
                        { "build",              "Mobile_iPhone" },
                        { "project",            "DSSAutomationFramework" },
                        {"name", "Iphone6SExecution"}
                       // { "realMobile",          "true"}
                    };

        }


        #endregion
        #endregion

    }
    
}
