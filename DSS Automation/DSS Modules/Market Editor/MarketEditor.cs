﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Configuration;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium;


namespace DSS_Automation
{
    [TestClass]
    public class MarketEditor
    {
        public string supportedBrowser { get; set; }
        public string sessionName { get; set; }
        public Startpage start_object;
        [TestInitialize]
        public void InitiateTest()
        {
            supportedBrowser = "iPad_Pro";
            sessionName = "MarketEditor check";
            SetUpBrowserStackDriver();
            start_object = new Startpage(driver);
            start_object.startpageMethod(driver, "Content Editor", @"vccbiz\ruthayak", "Apr@2018");
        }

        [TestMethod]
        public void marketEditorCheck()
        {
            IWebElement logo = driver.FindElement(By.XPath("//*[@id='globalLogo']"));
            logo.Click();
            System.Threading.Thread.Sleep(10000);
            IWebElement contentEditorItem = driver.FindElement(By.XPath("/html/body/div[1]/div/section/section/div/div/div[1]/div[1]/div[1]/a[1]/span[1]/img"));
            contentEditorItem.Click();
            System.Threading.Thread.Sleep(5000);
            //settings item selection
            driver.FindElement(By.XPath("//div[@id='ContentTreeActualSize']//a[contains(@id,'Tree_Node_13D6D6C6C50B4BBDB3312B04F1A58F21')]/parent::node()/img")).Click();
            //system
            driver.FindElement(By.XPath("//*[@id='Tree_Glyph_504AE1899F364C62976766D73D6C3084']")).Click();
            driver.FindElement(By.XPath("//*[@id='Tree_Glyph_6C7822CC4C154E3C8C45C7D3BDF373DD']")).Click();
            driver.FindElement(By.XPath("//*[@id='Tree_Glyph_E27B262D8731476B95AC6D3A7A2E6EE2']")).Click();
            driver.FindElement(By.XPath("//*[@id='Tree_Node_EEA4609F6F7441C182C653916D572A28']/span")).Click();
            System.Threading.Thread.Sleep(1000);
            string editInfo = driver.FindElement(By.XPath("//*[@id='EditorPanelEEA4609F6F7441C182C653916D572A28']/div[2]/div[2]/div[1]")).GetAttribute("innerHTML");
            //string editInfo1 = driver.FindElement(By.XPath("//*[@id='EditorPanelEEA4609F6F7441C182C653916D572A28']/div[2]/div[2]/div[1]")).GetAttribute("Text");
            //string editInfo2 = driver.FindElement(By.XPath("//*[@id='EditorPanelEEA4609F6F7441C182C653916D572A28']/div[2]/div[2]/div[1]")).GetAttribute("innerText");
            Assert.IsTrue(editInfo.Equals("You cannot edit this item because you do not have write access to it."));
        }


        [TestCleanup]
        public void cleanup()
        {
            driver.Quit();
        }

        #region Browserstack
        // public string env = ConfigurationManager.AppSettings["Environment"];

        //Login for Sitecore for Authoring
        public const string usernameTextBox = ".//*[@id='UserName']";
        public const string passwordTextBox = ".//*[@id='Password']";
        public const string loginButton = ".//*[@id='login']/input";
        //BrowserStack credencials

        #region BrowserStack Configurations

        /* public string GetTestMethodName(string name)
        {
            return TestContext.TestName;

        }*/
        public string BrowserStackUserValue { get; set; }
        public string BrowserStackKey { get; set; }
        public string methodName;
        //  public TestContext TestContext { get; set; }
        //Web or Mobile version to Launch
        //public string supportedBrowser = ConfigurationManager.AppSettings["TargetBrowser"];
        public Dictionary<string, object> BrowserCapability { get; set; }

        public IWebDriver driver;

        //public Driver()
        //{
        //    BrowserStackUser = ConfigurationManager.AppSettings["BrowserStackUser"];
        //    BrowserStackKey = ConfigurationManager.AppSettings["BroserStackKey"];
        //    //Initiale all the properties of Browser that need to be launched
        //    InitialiseBrowser(supportedBrowser);
        //    //Initialise and declare desired capability
        //    DesiredCapabilities capability = new DesiredCapabilities(BrowserCapability);


        //    //Global driver to launch Browser stack with username and Accesskey
        //    driver = new RemoteWebDriver(
        //   new Uri("http://hub-cloud.browserstack.com/wd/hub/"), capability
        // );

        //}

        public void SetUpBrowserStackDriver()
        {
            BrowserStackUserValue = ConfigurationManager.AppSettings["BrowserStackUser"];
            BrowserStackKey = ConfigurationManager.AppSettings["BroserStackKey"];
            //Initiale all the properties of Browser that need to be launched
            InitialiseBrowser(supportedBrowser);
            //Initialise and declare desired capability
            DesiredCapabilities capability = new DesiredCapabilities(BrowserCapability);


            //Global driver to launch Browser stack with username and Accesskey
            driver = new RemoteWebDriver(
           new Uri("http://hub-cloud.browserstack.com/wd/hub/"), capability
         );

        }

        public void InitialiseBrowser(string supportedBrowser)
        {
            //BrowserStackUserValue = ConfigurationManager.AppSettings["BrowserStackUser"];
            //BrowserStackKey = ConfigurationManager.AppSettings["BroserStackKey"];
            //DesiredCapabilities capability = new DesiredCapabilities(BrowserCapability);
            //driver = new RemoteWebDriver(
            //    new Uri("http://hub-cloud.browserstack.com/wd/hub/"), capability
            //    );
            switch (supportedBrowser)
            {
                case "Chrome_Browser":
                    //Tests.HomePageTest.GetTestMethodName();
                    ChromeNavigatorOnBrowserStack();
                    break;

                case "Firefox_Browser":
                    FirefoxNavigator();
                    break;

                case "iPhone6":
                    iPhone6NavigatorOnBrowserStack();
                    break;
                case "iPadMini":
                    iPadMiniNavigatorOnBrowserStack();
                    break;
                case "Galaxy":
                    AndriodGalaxyTabOnBrowserStack();
                    break;
                case "iPad_Pro":
                    iPadProNavigatorOnBrowserStack();
                    break;

                default:
                    FirefoxNavigator();
                    break;
            }
        }

        private void FirefoxNavigator()
        {
            BrowserCapability = new Dictionary<string, object>() {
                        { "browserstack.user",     BrowserStackUserValue },
                        { "browserstack.key",      BrowserStackKey },
                        //{ "browserstack.local",    "true" },
                        { "browserstack.debug",    "true" },//for screenshots
                        { "browser",              "FireFox" },
                        { "browser_version",      "49.0" },
                        { "os",                   "Windows" },
                        { "os_version",           "10" },
                       // { "resolution",          "2048x1536" },
                        { "build",              "00001" },
                        { "project",            "DSSAutomationFramework" }
                    };

        }

        private void iPhone6NavigatorOnBrowserStack()
        {
            BrowserCapability = new Dictionary<string, object>() {
                        { "browserstack.user",     BrowserStackUserValue },
                        { "browserstack.key",      BrowserStackKey },
                        { "browserstack.local",    "false" },
                        { "browserstack.debug",    "true" },
                        {"platform", "MAC"},
                        { "device",             "iPhone 6S Plus" },
                        { "os",                   "iOS" },
                        { "build",             "Regression_Parallelthreadstrial" },
                        { "project",            "DSSAutomationFramework" },
                        {"name", "Iphone6" }
                       // { "realMobile", "true"}
                    };

        }

        private void ChromeNavigatorOnBrowserStack()
        {
            BrowserCapability = new Dictionary<string, object>() {
                        { "browserstack.user",     BrowserStackUserValue },
                        { "browserstack.key",      BrowserStackKey },
                        { "browserstack.local",    "False" },
                        { "browserstack.debug",    "true" },
                        { "browser",              "Chrome" },
                        { "browser_version",      "54.0" },
                        { "os",                   "Windows" },
                        { "os_version",           "10" },
                        {"quality",        "Compressed" },
                        { "build",              "Regression_DSS" },
                        { "project",            "DSSAutomationFramework" },
                        {"resolution","1920x1080"},
                        {"name",sessionName }

                    };
        }
        private void iPadProNavigatorOnBrowserStack()
        {
            BrowserCapability = new Dictionary<string, object>() {
                        { "browserstack.user",     BrowserStackUserValue },
                        { "browserstack.key",      BrowserStackKey },
                        { "os_version",    "11.2" },
                        { "device",    "iPad Pro" },
                        {"real_mobile", "true"},
                        { "browserstack.local",    "False" },
                        { "browserstack.debug",    "true" },
                        { "deviceOrientation", "landscape" },
                        { "build", "Regression_DSS" },
                        { "project",            "DSSAutomationFramework" },
                        {"name", "iPad_Pro Result"},
                        {"browserstack.appium_version","1.7.0"},
                        {"platform","MAC" },
                        {"browserstack.video","true"},
                        {"acceptSslCert","false" }
                    };
        }
        private void Chrome1NavigatorOnBrowserStack()
        {
            BrowserCapability = new Dictionary<string, object>() {
                        { "browserstack.user",     BrowserStackUserValue },
                        { "browserstack.key",      BrowserStackKey },
                        //{ "browserstack.local",    "true" },
                        { "browserstack.debug",    "true" },
                        { "browser",              "Chrome" },
                        { "browser_version",      "54.0" },
                        { "os",                   "Windows" },
                        { "os_version",           "8.1" },
                        {"quality",        "Compressed" },
                        { "build",              "Chrome" },
                        { "project",            "DSSAutomationFramework" },
                        {"resolution","1280x800"}
                    };
        }

        private void AndriodGalaxyTabOnBrowserStack()
        {
            BrowserCapability = new Dictionary<string, object>() {
                        { "browserstack.user",     BrowserStackUserValue },
                        { "browserstack.key",      BrowserStackKey },
                        { "browserstack.local",    "true" },
                        { "browserstack.debug",    "true" },
                        { "device",             "Samsung Galaxy S6" },
                        { "os",                   "Android" },
                        { "build",              "Demo_Galaxy" },
                        { "project",            "DSSAutomationFramework" },
                        {"name", "Demo_Home"},
                        {"deviceOrientation","portrait"},
                        {"realMobile","true"}
                    };

        }

        private void iPadMiniNavigatorOnBrowserStack()
        {
            BrowserCapability = new Dictionary<string, object>() {
                        { "browserstack.user",     BrowserStackUserValue },
                        { "browserstack.key",      BrowserStackKey },
                        { "browserstack.local",    "false" },
                        { "browserstack.debug",    "true" },
                        {"platform", "MAC"},
                        { "device",             "iPad Mini" },
                        { "os",                   "iOS" },
                        { "build",              "Mobile_iPhone" },
                        { "project",            "DSSAutomationFramework" },
                        {"name", "Iphone6SExecution"}
                       // { "realMobile",          "true"}
                    };

        }



        #endregion
        #endregion


    }
}




