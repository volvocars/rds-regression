﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using System;
using OpenQA.Selenium.Chrome;

namespace DSS_Automation
{
    [TestClass]
    public class ShowroomAdminVSCallMethod
    {
        public Sprint1_Object S1_Object;
        public Startpage startPage_Object;
        public IWebDriver driver;


        [TestInitialize]
        public void InitiateTest()
        {
            driver = new ChromeDriver();
            startPage_Object = new Startpage(driver);
            S1_Object = new Sprint1_Object(driver);
            string userName = Convert.ToString(TestContext.DataRow["Showroom admin User name"]);
            string passWord = Convert.ToString(TestContext.DataRow["Showroom admin password"]);
            startPage_Object.startpageMethod(driver, "Showroom Admin", userName, passWord);

        }

        private TestContext testContextInstance;
        public TestContext TestContext
        {
            get { return testContextInstance; }
            set { testContextInstance = value; }
        }


        [DataSource("System.Data.Odbc", "Dsn=Excel Files;Driver={Microsoft Excel Driver (*.xls)};dbq=|DataDirectory|\\Test Data_VS.xlsx;defaultdir=.;driverid=790;maxbuffersize=2048;pagetimeout=5;readonly=true", "Sheet2$", DataAccessMethod.Sequential)]
        [TestMethod]
        [TestCategory("Showroom Admin[Functional Validation]")]
        public void uploadImageEditMethod()
        {

            string carUpperSuffixValue = Convert.ToString(TestContext.DataRow["Car Upper suffix"]);
            string secondaryTitleEdit = Convert.ToString(TestContext.DataRow["Secondary title Edit"]);
            string Showroom = Convert.ToString(TestContext.DataRow["Showroom"]);
            S1_Object.showroomSelect(driver, Showroom);
            S1_Object.energyDecJPGUpload(driver, carUpperSuffixValue, secondaryTitleEdit);
        }

        [TestCleanup]
        public void ShowroomAdminCleanup()
        {
            driver.Quit();
        }


    }
}
