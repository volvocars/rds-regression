﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Configuration;
using OpenQA.Selenium.Remote;

namespace DSS_Automation
{
    [TestClass]
    public class ShowroomAdminCallMethod
    {
        //Driver instance creation required if the scripts are required to be run through visual studio and not through browserstack
        //IWebDriver driver = new ChromeDriver();
        public Sprint1_Object S1_Object;
        public Startpage startPage_Object;
        public string supportedBrowser { get; set; }
        public string sessionName { get; set; }




        [TestInitialize]
        public void InitiateTest()
        {
            supportedBrowser = Convert.ToString(TestContext.DataRow["Browser Name"]);
            sessionName = Convert.ToString(TestContext.DataRow["Session Name"]);
            string userName = Convert.ToString(TestContext.DataRow["Showroom admin User name"]);
            string passWord = Convert.ToString(TestContext.DataRow["Showroom admin password"]);

            //string showroomSelect = Convert.ToString(TestContext.DataRow["Showroom"]);
            //Browserstack S2 object creation
            SetUpBrowserStackDriver();
            startPage_Object = new Startpage(driver);
            S1_Object = new Sprint1_Object(driver);
            startPage_Object.startpageMethod(driver, "Showroom Admin", userName, passWord);
        }

        #region Showroom Admin Call Methods

        private TestContext testContextInstance;
        public TestContext TestContext
        {
            get { return testContextInstance; }
            set { testContextInstance = value; }
        }

        //To create a new showroom car in showroom admin page
        [DataSource("System.Data.Odbc", "Dsn=Excel Files;Driver={Microsoft Excel Driver (*.xls)};dbq=|DataDirectory|\\Test Data_VS.xlsx;defaultdir=.;driverid=790;maxbuffersize=2048;pagetimeout=5;readonly=true", "Sheet2$", DataAccessMethod.Sequential)]
        [TestMethod]
        [TestCategory("Showroom Admin[Functional Validation]")]
        public void createNewShowroomcarCallMethod()
        {
            string carModelTemplateValue = Convert.ToString(TestContext.DataRow["Car Model"]);
            string marketLanguageDDValue = Convert.ToString(TestContext.DataRow["Market"]);
            string languageDDLValue = Convert.ToString(TestContext.DataRow["Language"]);
            string carTitleValue = Convert.ToString(TestContext.DataRow["Car Title"]);
            string carUpperSuffixValue = Convert.ToString(TestContext.DataRow["Car Upper suffix"]);
            string carLowerSuffixValue = Convert.ToString(TestContext.DataRow["Car Lower suffix"]);
            string carSecondaryTitleValue = Convert.ToString(TestContext.DataRow["Car Secondary title"]);
            string priceValue = Convert.ToString(TestContext.DataRow["Price as shown"]);
            string startingAtValue = Convert.ToString(TestContext.DataRow["Starting at"]);
            string vinNumberValue = Convert.ToString(TestContext.DataRow["VIN Number"]);
            string keyHighlightsTitleValue = Convert.ToString(TestContext.DataRow["key highlights Title"]);
            string keyhighlightsdescValue = Convert.ToString(TestContext.DataRow["Key highlights description"]);
            string showroomNameValue = Convert.ToString(TestContext.DataRow["Showroom Name"]);
            string secondaryTitleEdit = Convert.ToString(TestContext.DataRow["Secondary title Edit"]);
            string Showroom = Convert.ToString(TestContext.DataRow["Showroom"]);
            S1_Object.showroomSelect(driver, Showroom);
            S1_Object.createNewCarMethod(Showroom, carModelTemplateValue, marketLanguageDDValue, languageDDLValue, carTitleValue, carUpperSuffixValue, carLowerSuffixValue, carSecondaryTitleValue, priceValue, startingAtValue, vinNumberValue, keyHighlightsTitleValue, keyhighlightsdescValue, showroomNameValue, driver);
        }

        //Calls the edit showroom car method in Sprint1_object class from Sprint2 class
        //To create a new showroom car in showroom admin page

        [DataSource("System.Data.Odbc", "Dsn=Excel Files;Driver={Microsoft Excel Driver (*.xls)};dbq=|DataDirectory|\\Test Data_VS.xlsx;defaultdir=.;driverid=790;maxbuffersize=2048;pagetimeout=5;readonly=true", "Sheet2$", DataAccessMethod.Sequential)]
        [TestMethod]
        [TestCategory("Showroom Admin[Functional Validation]")]
        public void editShowroomCarCallMethod()
        {
            string carUpperSuffixValue = Convert.ToString(TestContext.DataRow["Car Upper suffix"]);
            string carSecondaryTitleValue = Convert.ToString(TestContext.DataRow["Car Secondary title"]);
            string secondaryTitleEdit = Convert.ToString(TestContext.DataRow["Secondary title Edit"]);
            string Showroom = Convert.ToString(TestContext.DataRow["Showroom"]);
            S1_Object.showroomSelect(driver, Showroom);
            //S1_Object.editShowroomCar(driver, carUpperSuffixValue, carSecondaryTitleValue, secondaryTitleEdit);
        }


        [DataSource("System.Data.Odbc", "Dsn=Excel Files;Driver={Microsoft Excel Driver (*.xls)};dbq=|DataDirectory|\\Test Data_VS.xlsx;defaultdir=.;driverid=790;maxbuffersize=2048;pagetimeout=5;readonly=true", "Sheet2$", DataAccessMethod.Sequential)]
        [TestMethod]
        [TestCategory("Showroom Admin[Functional Validation]")]
        public void uploadImageEditMethod()
        {
            string carUpperSuffixValue = Convert.ToString(TestContext.DataRow["Car Upper suffix"]);
            string secondaryTitleEdit = Convert.ToString(TestContext.DataRow["Secondary title Edit"]);
            string Showroom = Convert.ToString(TestContext.DataRow["Showroom"]);
            S1_Object.showroomSelect(driver, Showroom);
            S1_Object.energyDecJPGUpload(driver, carUpperSuffixValue, secondaryTitleEdit);
        }


        [TestMethod]
        [TestCategory("Showroom Admin[Functional Validation]")]
        public void allShowroomEditMethod()
        {
            S1_Object.editAllShowroomCarCallMethod(driver);
        }





        //Calls the publish showroom car method in Sprint1_object class
        //To create a new showroom car in showroom admin page
        [DataSource("System.Data.Odbc", "Dsn=Excel Files;Driver={Microsoft Excel Driver (*.xls)};dbq=|DataDirectory|\\Test Data_VS.xlsx;defaultdir=.;driverid=790;maxbuffersize=2048;pagetimeout=5;readonly=true", "Sheet2$", DataAccessMethod.Sequential)]
        [TestMethod]
        [TestCategory("Showroom Admin[Functional Validation]")]
        public void publishCarCallMethod()
        {
            string carUpperSuffixValue = Convert.ToString(TestContext.DataRow["Car Upper suffix"]);
            string secondaryTitleEdit = Convert.ToString(TestContext.DataRow["Secondary title Edit"]);
            string Showroom = Convert.ToString(TestContext.DataRow["Showroom"]);
            S1_Object.showroomSelect(driver, Showroom);
            S1_Object.publishCar(driver, carUpperSuffixValue, secondaryTitleEdit);
        }



        [TestCleanup]
        public void ShowroomAdminCleanup()
        {
            driver.Quit();
        }
        #endregion
        #region Browserstack
        // public string env = ConfigurationManager.AppSettings["Environment"];

        //Login for Sitecore for Authoring
        public const string usernameTextBox = ".//*[@id='UserName']";
        public const string passwordTextBox = ".//*[@id='Password']";
        public const string loginButton = ".//*[@id='login']/input";
        //BrowserStack credencials

        #region BrowserStack Configurations

        /* public string GetTestMethodName(string name)
        {
            return TestContext.TestName;

        }*/
        public string BrowserStackUserValue { get; set; }
        public string BrowserStackKey { get; set; }
        public string methodName;
        //  public TestContext TestContext { get; set; }
        //Web or Mobile version to Launch
        //public string supportedBrowser = ConfigurationManager.AppSettings["TargetBrowser"];
        public Dictionary<string, object> BrowserCapability { get; set; }

        public RemoteWebDriver driver;

        //public Driver()
        //{
        //    BrowserStackUser = ConfigurationManager.AppSettings["BrowserStackUser"];
        //    BrowserStackKey = ConfigurationManager.AppSettings["BroserStackKey"];
        //    //Initiale all the properties of Browser that need to be launched
        //    InitialiseBrowser(supportedBrowser);
        //    //Initialise and declare desired capability
        //    DesiredCapabilities capability = new DesiredCapabilities(BrowserCapability);


        //    //Global driver to launch Browser stack with username and Accesskey
        //    driver = new RemoteWebDriver(
        //   new Uri("http://hub-cloud.browserstack.com/wd/hub/"), capability
        // );

        //}

        public void SetUpBrowserStackDriver()
        {
            BrowserStackUserValue = ConfigurationManager.AppSettings["BrowserStackUser"];
            BrowserStackKey = ConfigurationManager.AppSettings["BroserStackKey"];
            //Initiale all the properties of Browser that need to be launched
            InitialiseBrowser(supportedBrowser);
            //Initialise and declare desired capability
            DesiredCapabilities capability = new DesiredCapabilities(BrowserCapability);


            //Global driver to launch Browser stack with username and Accesskey
            driver = new RemoteWebDriver(
           new Uri("http://hub-cloud.browserstack.com/wd/hub/"), capability

           );

            // driver.setFileDetector(new LocalFileDetector());

            LocalFileDetector detector = new LocalFileDetector();
            driver.FileDetector = detector;

        }

        public void InitialiseBrowser(string supportedBrowser)
        {
            switch (supportedBrowser)
            {
                case "Chrome_Browser":
                    //Tests.HomePageTest.GetTestMethodName();
                    ChromeNavigatorOnBrowserStack();
                    break;

                case "Firefox_Browser":
                    FirefoxNavigator();
                    break;

                case "iPhone6":
                    iPhone6NavigatorOnBrowserStack();
                    break;
                case "iPadMini":
                    iPadMiniNavigatorOnBrowserStack();
                    break;
                case "Galaxy":
                    AndriodGalaxyTabOnBrowserStack();
                    break;

                default:
                    FirefoxNavigator();
                    break;
            }
        }

        private void FirefoxNavigator()
        {
            BrowserCapability = new Dictionary<string, object>() {
                        { "browserstack.user",     BrowserStackUserValue },
                        { "browserstack.key",      BrowserStackKey },
                        //{ "browserstack.local",    "true" },
                        { "browserstack.debug",    "true" },//for screenshots
                        { "browser",              "FireFox" },
                        { "browser_version",      "49.0" },
                        { "os",                   "Windows" },
                        { "os_version",           "10" },
                       // { "resolution",          "2048x1536" },
                        { "build",              "00001" },
                        { "project",            "DSSAutomationFramework" }
                    };

        }

        private void iPhone6NavigatorOnBrowserStack()
        {
            BrowserCapability = new Dictionary<string, object>() {
                        { "browserstack.user",     BrowserStackUserValue },
                        { "browserstack.key",      BrowserStackKey },
                        { "browserstack.local",    "false" },
                        { "browserstack.debug",    "true" },
                        {"platform", "MAC"},
                        { "device",             "iPhone 6S Plus" },
                        { "os",                   "iOS" },
                        { "build",             "Regression_Parallelthreadstrial" },
                        { "project",            "DSSAutomationFramework" },
                        {"name", "Iphone6" }
                       // { "realMobile", "true"}
                    };

        }

        private void ChromeNavigatorOnBrowserStack()
        {
            BrowserCapability = new Dictionary<string, object>() {
                        { "browserstack.user",     BrowserStackUserValue },
                        { "browserstack.key",      BrowserStackKey },
                        { "browserstack.local",    "False" },
                        { "browserstack.debug",    "true" },
                        { "browser",              "Chrome" },
                        { "browser_version",      "65.0" },
                        { "os",                   "Windows" },
                        { "os_version",           "10" },
                         {"quality",        "Compressed" },
                        { "build",              "Regression_DSS" },
                        { "project",            "DSSAutomationFramework" },
                         {"resolution","2048x1536"},
                         {"name",sessionName}

                    };
        }
        private void Chrome1NavigatorOnBrowserStack()
        {
            BrowserCapability = new Dictionary<string, object>() {
                        { "browserstack.user",     BrowserStackUserValue },
                        { "browserstack.key",      BrowserStackKey },
                        //{ "browserstack.local",    "true" },
                        { "browserstack.debug",    "true" },
                        { "browser",              "Chrome" },
                        { "browser_version",      "54.0" },
                        { "os",                   "Windows" },
                        { "os_version",           "8.1" },
                        {"quality",        "Compressed" },
                        { "build",              "Chrome" },
                        { "project",            "DSSAutomationFramework" },
                        {"resolution","1280x800"}
                    };
        }

        private void AndriodGalaxyTabOnBrowserStack()
        {
            BrowserCapability = new Dictionary<string, object>() {
                        { "browserstack.user",     BrowserStackUserValue },
                        { "browserstack.key",      BrowserStackKey },
                        { "browserstack.local",    "true" },
                        { "browserstack.debug",    "true" },
                        { "device",             "Samsung Galaxy S6" },
                        { "os",                   "Android" },
                        { "build",              "Demo_Galaxy" },
                        { "project",            "DSSAutomationFramework" },
                        {"name", "Demo_Home"},
                        {"deviceOrientation","portrait"},
                        {"realMobile","true"}
                    };

        }

        private void iPadMiniNavigatorOnBrowserStack()
        {
            BrowserCapability = new Dictionary<string, object>() {
                        { "browserstack.user",     BrowserStackUserValue },
                        { "browserstack.key",      BrowserStackKey },
                        { "browserstack.local",    "false" },
                        { "browserstack.debug",    "true" },
                        {"platform", "MAC"},
                        { "device",             "iPad Mini" },
                        { "os",                   "iOS" },
                        { "build",              "Mobile_iPhone" },
                        { "project",            "DSSAutomationFramework" },
                        {"name", "Iphone6SExecution"}
                       // { "realMobile",          "true"}
                    };

        }


        #endregion
        #endregion


    }
}
