﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Windows.Forms;

namespace DSS_Automation
{
    [TestClass]
    public class Sprint1_Object
    {
        public Common co_object;
        public Sprint1_Object(IWebDriver Wdriver)
        {
            PageFactory.InitElements(Wdriver, this);
            co_object = new Common();

        }

        #region pageObjects
        //TS1
        [FindsBy(How = How.Id, Using = "DealerName")]
        public IWebElement showroomName { get; set; }

        [FindsBy(How = How.XPath, Using = "//input[contains(@class,'checkbox dealer-hide')]")]
        public IWebElement hideCheckBox { get; set; }


        //TS2

        [FindsBy(How = How.XPath, Using = "//button[contains(text(),'Create New Car')]")]
        public IWebElement createNewCar { get; set; }

        [FindsBy(How = How.Id, Using = "ModelTemplateSelectedId")]
        public IWebElement carModelTemplate { get; set; }

        [FindsBy(How = How.Id, Using = "ModelTemplateLanguageSelectedId")]
        public IWebElement marketDD { get; set; }

        [FindsBy(How = How.Id, Using = "ShowroomLanguages")]
        public IWebElement languageDDID { get; set; }

        [FindsBy(How = How.XPath, Using = "//button[contains(@class,'btn dropdown-toggle bs-placeholder btn-default')]")]
        public IWebElement languageDD { get; set; }

        [FindsBy(How = How.XPath, Using = "//a/span[contains(text(),'en-GB')]")]
        public IWebElement languageDDSelection { get; set; }

        [FindsBy(How = How.XPath, Using = "//div[contains(@class,'btn-group bootstrap-select show-tick selectshowroomLanguage dd-close')]/button/span[contains(text(),'en-GB')]")]
        public IWebElement languageDDEnter { get; set; }

        [FindsBy(How = How.Id, Using = "CarTitle")]
        public IWebElement carTitle { get; set; }

        [FindsBy(How = How.Id, Using = "CarUpperSuffixTitle")]
        public IWebElement carUpperSuffix { get; set; }

        [FindsBy(How = How.Id, Using = "CarLowerSuffixTitle")]
        public IWebElement carLowerSuffix { get; set; }

        [FindsBy(How = How.Id, Using = "CarSecondaryTitle")]
        public IWebElement carSecondaryTitle { get; set; }

        [FindsBy(How = How.Id, Using = "Price")]
        public IWebElement price { get; set; }

        [FindsBy(How = How.Id, Using = "StartingPrice")]
        public IWebElement startingPrice { get; set; }

        [FindsBy(How = How.Id, Using = "VINNumber")]
        public IWebElement vinNumber { get; set; }

        [FindsBy(How = How.XPath, Using = "//button[contains(text(),' Search')]")]
        public IWebElement searchButton { get; set; }

        [FindsBy(How = How.Id, Using = "SelectedModelyear")]
        public IWebElement modelYear { get; set; }

        [FindsBy(How = How.Id, Using = "ModelTemplateVariantSelectedId")]
        public IWebElement modelVariant { get; set; }

        [FindsBy(How = How.Id, Using = "KeyHighlightsTitle")]
        public IWebElement keyHighlightsTitle { get; set; }

        [FindsBy(How = How.XPath, Using = "//div/div/button[contains(text(),'Save')]")]
        public IWebElement save { get; set; }

        [FindsBy(How = How.XPath, Using = "//button[@class='button button-small js-dss-cancel']")]
        public IWebElement cancel { get; set; }

        [FindsBy(How = How.XPath, Using = "//button[@class='button-small button-opaque js-publish-all']")]
        public IWebElement publishAllCarsCTA { get; set; }


        [FindsBy(How = How.XPath, Using = "//div[@class='energy-dec row']//span[@class='select-file']")]
        public IWebElement energyDecCTA { get; set; }

        [FindsBy(How = How.XPath, Using = "//button[@class='button button-small js-dss-savPub savePub-btn']")]
        public IWebElement saveAndPublishCTA { get; set; }

        [FindsBy(How = How.XPath, Using = "//input[@class='checkbox dealer-hide']")]
        public IWebElement showroomNameCheckBox { get; set; }
        #endregion

        #region locator value
        //Locator value
        string energyDecDelete = "//*[contains(@class,'energy-dec row')]//a[contains(text(),'Delete')]";
        string vinXpath = "//p[contains(text(),'VIN Lookup')]";
        string editShowroomPageHeader = "//h3[@class='admin-heading'][contains(text(),'Edit Showroom Car')]";
        string createShowroomPageHeader = "//h3[@class='admin-heading'][contains(text(),'Create new Showroom Car')]";
        string showroomCarsListPageHeader = "//h3[@class='admin-heading showroomHeader'][contains(text(),'List Of Showroom Cars')]";
        #endregion

        //Navigate to the desired showroom page from showrooms list page
        public void showroomSelect(IWebDriver driver, string showroomNameValue)
        {
            try
            {
                string showroomSelectXpath = "//*[span[contains(text(),'" + showroomNameValue + "')]]/parent::node()/following-sibling::div/p/a[contains(text(),'Select')]";
                driver.FindElement(By.XPath(showroomSelectXpath)).Click();
            }
            catch (ElementNotVisibleException)
            {
                var pageCount = driver.FindElements(By.XPath("//*[@id='page-nav']/ul/li")).Count();
                var nxtCount = pageCount - 1;
                for (var loopCount = 1; loopCount < nxtCount; loopCount++)
                {
                    try
                    {
                        driver.FindElement(By.XPath("//*[@id='page-nav']/ul/li/following-sibling::li/a[contains(text(),'Next')]")).Click();
                        string showroomSelectXpath = "//*[span[contains(text(),'" + showroomNameValue + "')]]/parent::node()/following-sibling::div/p/a[contains(text(),'Select')]";
                        driver.FindElement(By.XPath(showroomSelectXpath)).Click();
                    }
                    catch (ElementNotVisibleException)
                    {
                        continue;
                    }
                    break;
                }

            }
            catch (NullReferenceException)
            {
                var pageCount = driver.FindElements(By.XPath("//*[@id='page-nav']/ul/li")).Count();
                var nxtCount = pageCount - 1;
                for (var loopCount = 1; loopCount < nxtCount; loopCount++)
                {
                    try
                    {
                        driver.FindElement(By.XPath("//*[@id='page-nav']/ul/li/following-sibling::li/a[contains(text(),'Next')]")).Click();
                        string showroomSelectXpath = "//*[span[contains(text(),'" + showroomNameValue + "')]]/parent::node()/following-sibling::div/p/a[contains(text(),'Select')]";
                        driver.FindElement(By.XPath(showroomSelectXpath)).Click();
                    }
                    catch (NullReferenceException)
                    {
                        continue;
                    }
                    break;
                }

            }

        }

        //Create New showroom car
        [TestMethod]
        public void createNewCarMethod(string showroom, string carModelTemplateValue, string MarketLanguageDDValue, string languageDDLValue, string carTitleValue, string carUpperSuffixValue, string carLowerSuffixValue, string carSecondaryTitleValue, string PriceValue, string StartingAtValue, string VINNumberValue, string keyHighlightsTitleValue, string keyhighlightsdescValue, string showroomNameValue, IWebDriver driver)
        {
            co_object.waitForElementUntilVisible(showroomCarsListPageHeader, driver, 60);
            co_object.scrollToBottom(driver);
            createNewCar.Click();
            co_object.waitForElementUntilVisible(createShowroomPageHeader, driver, 60);

            //TS2
            SelectElement MarketDDL = new SelectElement(marketDD);
            MarketDDL.SelectByText(MarketLanguageDDValue);

            System.Threading.Thread.Sleep(3000);

            //languageDD.SendKeys("Enter");
            //languageDDSelection.Click();
            //languageDDEnter.Click();

            SelectElement LanguageDDL = new SelectElement(languageDDID);
            LanguageDDL.SelectByText(languageDDLValue);

            vinNumber.SendKeys(VINNumberValue);
            searchButton.Click();
            //System.Threading.Thread.Sleep(20000);
            co_object.waitForElementUntilVisible(vinXpath, driver, 80);

            SelectElement ModelTemplateDDL = new SelectElement(carModelTemplate);
            string ModelTemplateDDLSelectedOption = ModelTemplateDDL.SelectedOption.Text;
            if (ModelTemplateDDLSelectedOption == "")
            {
                ModelTemplateDDL.SelectByText(carModelTemplateValue);
            }

            SelectElement ModelYearDDL = new SelectElement(modelYear);
            var ModelYearDDLCount = ModelYearDDL.Options.Count;
            string ModelYearDDLSelectedOption = ModelYearDDL.SelectedOption.Text;
            if (ModelYearDDLSelectedOption == "")
            {
                if (ModelYearDDLCount != 0)
                {
                    ModelYearDDL.SelectByIndex(1);

                }

            }

            SelectElement ModelVariantDDL = new SelectElement(modelVariant);
            int ModelVariantDDlCount = ModelVariantDDL.Options.Count;
            if (ModelVariantDDlCount != 0)
            {
                ModelVariantDDL.SelectByIndex(1);
            }

            carTitle.SendKeys(carTitleValue);
            carUpperSuffix.SendKeys(carUpperSuffixValue);
            carLowerSuffix.SendKeys(carLowerSuffixValue);
            carSecondaryTitle.SendKeys(carSecondaryTitleValue);
            price.SendKeys(PriceValue);
            startingPrice.SendKeys(StartingAtValue);
            keyHighlightsTitle.SendKeys(keyHighlightsTitleValue);
            string[] keyHighlightDescList = keyhighlightsdescValue.Split(',');
            for (int keyHighlightDescIterator = 0; keyHighlightDescIterator < keyHighlightDescList.Length; keyHighlightDescIterator++)
            {
                keyHighlightDescList[keyHighlightDescIterator] = keyHighlightDescList[keyHighlightDescIterator].Trim();
                driver.FindElement(By.XPath("//input[@name='KeyHighlightsCollection[" + keyHighlightDescIterator + "].KeyHighlightsName']")).SendKeys(keyHighlightDescList[keyHighlightDescIterator]);
            }

            ((IJavaScriptExecutor)driver).ExecuteScript("window.scrollTo(0,document.body.scrollHeight)");
            save.Click();
        }

        [TestMethod]
        public void createNewCarPageValidation(IWebDriver driver, string showroom, string carModelTemplateValue, string MarketLanguageDDValue, string languageDDLValue, string VINNumberValue)
        {
            co_object.waitForElementUntilVisible(showroomCarsListPageHeader, driver, 60);
            co_object.scrollToBottom(driver);
            createNewCar.Click();
            co_object.waitForElementUntilVisible(createShowroomPageHeader, driver, 60);
            string prefilledShowroomValue = showroomName.GetAttribute("value");
            Assert.AreEqual(prefilledShowroomValue, showroom);
            string dealerCheckedValue = showroomNameCheckBox.GetCssValue("defaultChecked");
            Assert.Equals(dealerCheckedValue, "false");


        }





        //Edit the secondary title and save the changes.
        [TestMethod]
        public void editShowroomCarLinkSelect(IWebDriver driver, string USuffix, string Stitle, string Sectitle)
        {
            co_object.waitForElementUntilVisible(showroomCarsListPageHeader, driver, 60);
            string EditXpath = "//p[contains(text(),'" + USuffix + " " + Stitle + "')]/parent::node()/following-sibling::div/p/a[contains(text(),'Edit')]";
            co_object.MovetoElementMethod(driver, EditXpath);
            co_object.waitForElementUntilVisible(editShowroomPageHeader, driver, 60);
        }

        //Edit the car and upload an .jpg image
        [TestMethod]
        public void energyDecJPGUpload(IWebDriver driver, string USuffix, string Stitle)
        {
            co_object.ScrollToElementPosition(energyDecCTA, driver);
            IWebElement fileElement = driver.FindElement(By.XPath("//*[@id='myFile']"));
            var path = System.IO.Path.GetDirectoryName(Assembly.GetExecutingAssembly().GetName().CodeBase);
            path = path.Substring(6).Replace("\\bin\\Debug", "");
            var fullPath = path + "\\Car-Wallpaper-On-Wallpaper-Hd-6-2732x2048.jpg"; ;
            fileElement.SendKeys(fullPath);
            System.Threading.Thread.Sleep(1000);
            driver.FindElement(By.XPath("//*[@id='adminFormID']/div[2]/div[10]/div/div[1]/button")).Click();
            co_object.waitForElementUntilVisible(energyDecDelete, driver, 60);
        }


        //Edit the car and upload an .png image
        [TestMethod]
        public void energyDecPNGUpload(IWebDriver driver, string USuffix, string Stitle)
        {
            co_object.ScrollToElementPosition(energyDecCTA, driver);
            IWebElement fileElement = driver.FindElement(By.XPath("//*[@id='myFile']"));
            var path = System.IO.Path.GetDirectoryName(Assembly.GetExecutingAssembly().GetName().CodeBase);
            path = path.Substring(6).Replace("\\bin\\Debug", "");
            var fullPath = path + "\\CAS image.png"; ;
            fileElement.SendKeys(fullPath);
            System.Threading.Thread.Sleep(1000);
            driver.FindElement(By.XPath("//*[@id='adminFormID']/div[2]/div[10]/div/div[1]/button")).Click();
            co_object.waitForElementUntilVisible(energyDecDelete, driver, 60);
        }

        //Edit the car and upload an .png image
        [TestMethod]
        public void energyDecPDFUpload(IWebDriver driver, string USuffix, string Stitle)
        {
            co_object.ScrollToElementPosition(energyDecCTA, driver);
            IWebElement fileElement = driver.FindElement(By.XPath("//*[@id='myFile']"));
            var path = System.IO.Path.GetDirectoryName(Assembly.GetExecutingAssembly().GetName().CodeBase);
            path = path.Substring(6).Replace("\\bin\\Debug", "");
            var fullPath = path + "\\DSS TEST PDF.pdf"; ;
            fileElement.SendKeys(fullPath);
            System.Threading.Thread.Sleep(1000);
            driver.FindElement(By.XPath("//*[@id='adminFormID']/div[2]/div[10]/div/div[1]/button")).Click();
            co_object.waitForElementUntilVisible(energyDecDelete, driver, 60);
        }

        //save and publish the showroom car using "Save and publish" CTA
        [TestMethod]
        public void saveAndPublishCar(IWebDriver driver)
        {
            co_object.scrollToBottom(driver);
            saveAndPublishCTA.Click();
        }

        //Saves the showroom car
        [TestMethod]
        public void saveCarMethod(IWebDriver driver)
        {
            co_object.scrollToBottom(driver);
            save.Click();
        }

        //Cancel the changes in showroom car
        public void cancelChanges(IWebDriver driver)
        {
            co_object.scrollToBottom(driver);
            cancel.Click();
        }



        //Publish the edited showroom car from Sprint1TS3Method
        [TestMethod]
        public void publishCar(IWebDriver driver, string USuffix, string Sectitle)
        {
            co_object.waitForElementUntilVisible(showroomCarsListPageHeader, driver, 60);
            string PublishXPath = "//p[contains(text(),'" + USuffix + " " + Sectitle + "')]/parent::node()/following-sibling::div/p/a[contains(text(),'Publish')]";
            IWebElement publishLinkCTA = driver.FindElement(By.XPath(PublishXPath));
            co_object.ScrollToElementPosition(publishLinkCTA, driver);
            publishLinkCTA.Click();
        }



        //preview the pubished car
        [TestMethod]
        public string previewCar(IWebDriver driver, string USuffix, string Sectitle)
        {
            co_object.waitForElementUntilVisible(showroomCarsListPageHeader, driver, 60);
            string PreviewXPath = "//p[contains(text(),'" + USuffix + " " + Sectitle + "')]/parent::node()/following-sibling::div/p/a[contains(text(),'Preview')]";
            IWebElement previewLinkElement = driver.FindElement(By.XPath(PreviewXPath));
            System.Threading.Thread.Sleep(8000);
            string tabName = driver.WindowHandles.Last();
            driver.SwitchTo().Window(tabName);
            System.Threading.Thread.Sleep(8000);
            return PreviewXPath;
        }




        [TestMethod]
        public void editAllShowroomCarCallMethod(IWebDriver driver)
        {
            int showroomCount = driver.FindElements(By.XPath("//div[@class='paginationWrapper custom-row']//div[@class='row list-row']//p/a")).Count();
            for (int scIncrement = 25; scIncrement <= showroomCount; scIncrement++)
            {
                try
                {
                    driver.FindElement(By.XPath("//div[@class='paginationWrapper custom-row']//div[@class='row list-row'][" + scIncrement + "]//p/a")).Click();
                    //co_object.implicitWaitMethod(10, driver);
                    System.Threading.Thread.Sleep(10000);
                    int showroomCarCount = driver.FindElements(By.XPath("//div[@class='cars-list']//div[@class='paginationWrapper custom-row']//div[@class='row list-row']//p/a[contains(text(),'Edit')]")).Count();

                    for (int sCarIncrement = 1; sCarIncrement <= showroomCarCount; sCarIncrement++)
                    {
                        string editXPath = "//div[@class='cars-list']//div[@class='paginationWrapper custom-row']//div[@class='row list-row'][" + sCarIncrement + "]//p/a[contains(text(),'Edit')]";
                        co_object.MovetoElementMethod(driver, editXPath);
                        //co_object.implicitWaitMethod(20, driver);
                        System.Threading.Thread.Sleep(20000);
                        searchButton.Click();
                        //co_object.implicitWaitMethod(10, driver);
                        System.Threading.Thread.Sleep(10000);
                        string vinInputValue = vinNumber.GetAttribute("value");
                        if (vinInputValue != "")
                        {
                            IWebElement surePopup = driver.FindElement(By.XPath("/html/body/div[2]/div/div/div[2]/button[1]"));
                            if (surePopup.Displayed)
                            {
                                driver.FindElement(By.XPath("/html/body/div[2]/div/div/div[2]/button[1]")).Click();
                            }

                        }

                        //co_object.implicitWaitMethod(40, driver);
                        System.Threading.Thread.Sleep(40000);
                        IWebElement casErrorCode = driver.FindElement(By.XPath("//input[@id='CasErrorCode']"));
                        string errorCodeValue = casErrorCode.GetAttribute("value");
                        if (errorCodeValue == "500" || errorCodeValue == "200")
                        {
                            cancel.Click();
                            //co_object.implicitWaitMethod(20, driver);
                            System.Threading.Thread.Sleep(20000);
                        }
                        else
                        {
                            ((IJavaScriptExecutor)driver).ExecuteScript("window.scrollTo(0,document.body.scrollHeight)");
                            save.Click();
                            //co_object.implicitWaitMethod(40, driver);
                            System.Threading.Thread.Sleep(40000);
                            string publishStatus = driver.FindElement(By.XPath("//div[@class='cars-list']//div[@class='paginationWrapper custom-row']//div[@class='row list-row'][" + sCarIncrement + "]/div[7]")).GetAttribute("innerText");
                            if (publishStatus.Contains("Publish"))
                            {
                                driver.FindElement(By.XPath("//div[@class='cars-list']//div[@class='paginationWrapper custom-row']//div[@class='row list-row'][" + sCarIncrement + "]/div[7]")).Click();
                            }
                        }

                        //co_object.implicitWaitMethod(20, driver);
                        System.Threading.Thread.Sleep(20000);
                    }
                    driver.FindElement(By.XPath("//a[text()='Showroom Admin']")).Click();
                    driver.FindElement(By.XPath("//a[text()='Showrooms']")).Click();
                }
                catch (ElementNotVisibleException)
                {
                    var pageCount = driver.FindElements(By.XPath("//*[@id='page-nav']/ul/li")).Count();
                    var nxtCount = pageCount - 1;
                    for (var loopCount = 1; loopCount < nxtCount; loopCount++)
                    {
                        try
                        {
                            driver.FindElement(By.XPath("//*[@id='page-nav']/ul/li/following-sibling::li/a[contains(text(),'Next')]")).Click();
                            string showroomSelectXpath = "//div[@class='paginationWrapper custom-row']//div[@class='row list-row'][" + scIncrement + "]//p/a";
                            driver.FindElement(By.XPath(showroomSelectXpath)).Click();
                            System.Threading.Thread.Sleep(5000);
                            int showroomCarCount = driver.FindElements(By.XPath("//div[@class='cars-list']//div[@class='paginationWrapper custom-row']//div[@class='row list-row']//p/a[contains(text(),'Edit')]")).Count();
                            for (int sCarIncrement = 1; sCarIncrement <= showroomCarCount; sCarIncrement++)
                            {
                                string editXPath = "//div[@class='cars-list']//div[@class='paginationWrapper custom-row']//div[@class='row list-row'][" + sCarIncrement + "]//p/a[contains(text(),'Edit')]";
                                co_object.MovetoElementMethod(driver, editXPath); System.Threading.Thread.Sleep(12000);
                                searchButton.Click();
                                System.Threading.Thread.Sleep(1000);
                                string vinInputValue = vinNumber.GetAttribute("value");
                                if (vinInputValue != "")
                                {
                                    IWebElement surePopup = driver.FindElement(By.XPath("/html/body/div[2]/div/div/div[2]/button[1]"));
                                    if (surePopup.Displayed)
                                    {
                                        driver.FindElement(By.XPath("/html/body/div[2]/div/div/div[2]/button[1]")).Click();
                                    }

                                }
                                System.Threading.Thread.Sleep(15000);
                                IWebElement casErrorCode = driver.FindElement(By.XPath("//input[@id='CasErrorCode']"));
                                string errorCodeValue = casErrorCode.GetAttribute("value");
                                if (errorCodeValue == "500" || errorCodeValue == "200")
                                {
                                    cancel.Click();
                                    System.Threading.Thread.Sleep(20000);
                                }
                                else
                                {
                                    ((IJavaScriptExecutor)driver).ExecuteScript("window.scrollTo(0,document.body.scrollHeight)");
                                    save.Click();
                                    System.Threading.Thread.Sleep(50000);
                                    string publishStatus = driver.FindElement(By.XPath("//div[@class='cars-list']//div[@class='paginationWrapper custom-row']//div[@class='row list-row'][" + sCarIncrement + "]/div[7]")).GetAttribute("innerText");
                                    if (publishStatus.Contains("Publish"))
                                    {
                                        driver.FindElement(By.XPath("//div[@class='cars-list']//div[@class='paginationWrapper custom-row']//div[@class='row list-row'][" + sCarIncrement + "]/div[7]")).Click();
                                    }

                                }

                                System.Threading.Thread.Sleep(16000);
                            }
                            driver.FindElement(By.XPath("//a[text()='Showroom Admin']")).Click();
                            driver.FindElement(By.XPath("//a[text()='Showrooms']")).Click();

                        }
                        catch (ElementNotVisibleException)
                        {
                            continue;
                        }
                        break;
                    }
                    System.Threading.Thread.Sleep(3000);
                }
            }
        }
    }

}
