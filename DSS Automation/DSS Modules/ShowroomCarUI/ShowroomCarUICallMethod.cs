﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Configuration;
using OpenQA.Selenium.Remote;

namespace DSS_Automation
{
    [TestClass]
    public class ShowroomUICallMethod
    {
        public Sprint2_Object S2_Object;
        public string supportedBrowser { get; set; }
        public string sessionName { get; set; }
        public Startpage startPage_Object;

        [TestInitialize]
        public void InitiateTest()
        {
            supportedBrowser = Convert.ToString(TestContext.DataRow["Browser Name"]);
            sessionName = Convert.ToString(TestContext.DataRow["Session Name"]);
            string userName = Convert.ToString(TestContext.DataRow["Showroom admin User name"]);
            string passWord = Convert.ToString(TestContext.DataRow["Showroom admin password"]);
            //Browserstack object creation
            SetUpBrowserStackDriver();
            S2_Object = new Sprint2_Object(driver);
            startPage_Object = new Startpage(driver);
            startPage_Object.startpageMethod(driver, "Showroom Admin", userName, passWord);
        }

        private TestContext testContextInstance;
        public TestContext TestContext
        {
            get { return testContextInstance; }
            set { testContextInstance = value; }
        }

        #region Showroom Car UI call methods

        //Start page verification call method
        [TestMethod]
        [TestCategory("Showroom car")]
        public void startPageVerificationCallMethod()
        {
            string carTitleValue = Convert.ToString(TestContext.DataRow["Car Title"]);
            string carUpperSuffixValue = Convert.ToString(TestContext.DataRow["Car Upper suffix"]);
            string carSecondaryTitleValue = Convert.ToString(TestContext.DataRow["Car Secondary title"]);
            string priceValue = Convert.ToString(TestContext.DataRow["Price as shown"]);
            string startingAtValue = Convert.ToString(TestContext.DataRow["Starting at"]);
            string secondaryTitleEdit = Convert.ToString(TestContext.DataRow["Secondary title Edit"]);
            string carLowerSuffixValue = Convert.ToString(TestContext.DataRow["Car Lower suffix"]);
            string swipeTextValue = Convert.ToString(TestContext.DataRow["Car Lower suffix"]);
            S2_Object.startPageVerification(driver, carTitleValue, carUpperSuffixValue, carLowerSuffixValue, secondaryTitleEdit, priceValue, startingAtValue, swipeTextValue);
        }

        //Car details cover page verification call method
        [TestMethod]
        [TestCategory("Showroom car")]
        public void carDetailsCoverPageVerificationCallMethod()
        {
            string carTitleValue = Convert.ToString(TestContext.DataRow["Car Title"]);
            string carUpperSuffixValue = Convert.ToString(TestContext.DataRow["Car Upper suffix"]);
            string carLowerSuffixValue = Convert.ToString(TestContext.DataRow["Car Lower suffix"]);
            string keyHighlightsTitleValue = Convert.ToString(TestContext.DataRow["key highlights Title"]);
            string keyhighlightsdescValue = Convert.ToString(TestContext.DataRow["Key highlights description"]);
            string secondaryTitleEdit = Convert.ToString(TestContext.DataRow["Secondary title Edit"]);
            S2_Object.carDetailsCoverPageVerification(driver, carTitleValue, carUpperSuffixValue, carLowerSuffixValue, secondaryTitleEdit, keyHighlightsTitleValue, keyhighlightsdescValue);
        }

        //Car details details page verification call method
        [TestMethod]
        [TestCategory("Showroom car")]
        public void carDetailsdetailPageVerificationCallMethod()
        {
            string priceValue = Convert.ToString(TestContext.DataRow["Price as shown"]);
            string startingAtValue = Convert.ToString(TestContext.DataRow["Starting at"]);
            S2_Object.carDetailsdetailPageVerification(driver, priceValue, startingAtValue);
        }

        //Technical Specifications overlay verification call method
        [TestMethod]
        [TestCategory("Showroom car")]
        public void techSpecificationOverlayVerificationCallMethod()
        {
            string technicalSpecificationPageTitleText = Convert.ToString(TestContext.DataRow["Technical specifications page title Text"]);
            string technicalSpecificationCTAText = Convert.ToString(TestContext.DataRow["Technical specifications CTA text"]);
            string technicalSpecificationCDDHeaderText = Convert.ToString(TestContext.DataRow["Technical specifications car details page header text"]);
            S2_Object.techSpecificationOverlayVerification(driver, technicalSpecificationPageTitleText, technicalSpecificationCTAText, technicalSpecificationCDDHeaderText);
        }

        //Equipment & features overlay verification call method
        [TestMethod]
        [TestCategory("Showroom car")]
        public void EquipmentOverlayVerificationCallMethod()
        {
            S2_Object.EquipmentOverlayVerification(driver);
        }

        //Test Drive CTA verification in Car details details page call method
        [TestMethod]
        [TestCategory("Showroom car")]
        public void testDriveOverlayCallMethod()
        {
            string testDriveCTAText = Convert.ToString(TestContext.DataRow["Test Drive CTA text"]);
            string testDrivePageTitleText = Convert.ToString(TestContext.DataRow["Test Drive Page Title Text"]);
            S2_Object.testDriveOverlay(driver, testDriveCTAText, testDrivePageTitleText);
        }

        //Share by email verification in car details - details page call method
        [TestMethod]
        [TestCategory("Showroom car")]
        public void shareByEmailOverlayCallMethod()
        {
            string shareByEmailCTAText = Convert.ToString(TestContext.DataRow["Share by email CTA Text"]);
            string shareByEmailPageTitleText = Convert.ToString(TestContext.DataRow["Share by email page title"]);
            S2_Object.shareByEmailOverlay(driver,shareByEmailCTAText,shareByEmailPageTitleText);
        }

        //Energy declaration cover page verification call method
        [TestMethod]
        [TestCategory("Showroom car")]
        public void EnergyDecCallMethod()
        {
            string carTitleValue = Convert.ToString(TestContext.DataRow["Car Title"]);
            string carUpperSuffixValue = Convert.ToString(TestContext.DataRow["Car Upper suffix"]);
            string carLowerSuffixValue = Convert.ToString(TestContext.DataRow["Car Lower suffix"]);
            string secondaryTitleEdit = Convert.ToString(TestContext.DataRow["Secondary title Edit"]);
            string energyDeclarationTitleText = Convert.ToString(TestContext.DataRow["Energy declaration title text"]);
            S2_Object.EnergydeclarationMethod(driver,carTitleValue, carUpperSuffixValue,carLowerSuffixValue,secondaryTitleEdit,energyDeclarationTitleText);
        }

        //Media Cover page verification call method
        [TestMethod]
        [TestCategory("Showroom car")]
        public void MediaCallMethod()
        {
            string mediaTitleText = Convert.ToString(TestContext.DataRow["Media titlte text"]);
            string testDriveCTAText = Convert.ToString(TestContext.DataRow["Test Drive CTA text"]);
            string shareByEmailCTAText = Convert.ToString(TestContext.DataRow["Share by email CTA Text"]);
            S2_Object.MediaCoverPageMethod(driver,mediaTitleText,testDriveCTAText,shareByEmailCTAText);
        }

        //Featured Videos page verification call method
        [TestMethod]
        [TestCategory("Showroom car")]
        public void FeaturedVideoCallMethod()
        {
            string featuredVideosTitleText = Convert.ToString(TestContext.DataRow["Featured videos Title"]);
            string featuedVideoDisclaimertext = Convert.ToString(TestContext.DataRow["featured video Disclaimer"]);
            S2_Object.FeaturedVideoPageMethod(driver,featuredVideosTitleText,featuedVideoDisclaimertext);
        }

        //Gallery Page verification call method
        [TestMethod]
        [TestCategory("Showroom car")]
        public void GalleryPageCallMethod()
        {
            string galleryTitleText = Convert.ToString(TestContext.DataRow["Gallery title"]);
            string gallerySubHeaderText = Convert.ToString(TestContext.DataRow["Gallery sub header"]);
            S2_Object.GalleryPageMethod(driver,galleryTitleText,gallerySubHeaderText);
        }

        //Footer page verification call method
        [TestMethod]
        [TestCategory("Showroom car")]
        public void FooterPageCallMethod()
        {
            string testDriveCTAText = Convert.ToString(TestContext.DataRow["Test Drive CTA text"]);
            string shareByEmailCTAText = Convert.ToString(TestContext.DataRow["Share by email CTA Text"]);
            string backToStartCTAText = Convert.ToString(TestContext.DataRow["Back to start"]);
            S2_Object.footerpageMethod(driver,backToStartCTAText,testDriveCTAText,shareByEmailCTAText);
        }

        [TestCleanup]
        public void showroomUICleanup()
        {
            driver.Quit();
        }

        #endregion
        #region Browserstack
        // public string env = ConfigurationManager.AppSettings["Environment"];

        //Login for Sitecore for Authoring
        public const string usernameTextBox = ".//*[@id='UserName']";
        public const string passwordTextBox = ".//*[@id='Password']";
        public const string loginButton = ".//*[@id='login']/input";
        //BrowserStack credencials

        #region BrowserStack Configurations

        /* public string GetTestMethodName(string name)
        {
            return TestContext.TestName;

        }*/
        public string BrowserStackUserValue { get; set; }
        public string BrowserStackKey { get; set; }
        public string methodName;
        //  public TestContext TestContext { get; set; }
        //Web or Mobile version to Launch
        //public string supportedBrowser = ConfigurationManager.AppSettings["TargetBrowser"];
        public Dictionary<string, object> BrowserCapability { get; set; }

        public IWebDriver driver;

        //public Driver()
        //{
        //    BrowserStackUser = ConfigurationManager.AppSettings["BrowserStackUser"];
        //    BrowserStackKey = ConfigurationManager.AppSettings["BroserStackKey"];
        //    //Initiale all the properties of Browser that need to be launched
        //    InitialiseBrowser(supportedBrowser);
        //    //Initialise and declare desired capability
        //    DesiredCapabilities capability = new DesiredCapabilities(BrowserCapability);


        //    //Global driver to launch Browser stack with username and Accesskey
        //    driver = new RemoteWebDriver(
        //   new Uri("http://hub-cloud.browserstack.com/wd/hub/"), capability
        // );

        //}

        public void SetUpBrowserStackDriver()
        {
            BrowserStackUserValue = ConfigurationManager.AppSettings["BrowserStackUser"];
            BrowserStackKey = ConfigurationManager.AppSettings["BroserStackKey"];
            //Initiale all the properties of Browser that need to be launched
            InitialiseBrowser(supportedBrowser);
            //Initialise and declare desired capability
            DesiredCapabilities capability = new DesiredCapabilities(BrowserCapability);


            //Global driver to launch Browser stack with username and Accesskey
            driver = new RemoteWebDriver(
           new Uri("http://hub-cloud.browserstack.com/wd/hub/"), capability
         );

        }

        public void InitialiseBrowser(string supportedBrowser)
        {
            switch (supportedBrowser)
            {
                case "Chrome_Browser":
                    //Tests.HomePageTest.GetTestMethodName();
                    ChromeNavigatorOnBrowserStack();
                    break;

                case "Firefox_Browser":
                    FirefoxNavigator();
                    break;

                case "iPhone6":
                    iPhone6NavigatorOnBrowserStack();
                    break;
                case "iPadMini":
                    iPadMiniNavigatorOnBrowserStack();
                    break;
                case "Galaxy":
                    AndriodGalaxyTabOnBrowserStack();
                    break;

                default:
                    FirefoxNavigator();
                    break;
            }
        }

        private void FirefoxNavigator()
        {
            BrowserCapability = new Dictionary<string, object>() {
                        { "browserstack.user",     BrowserStackUserValue },
                        { "browserstack.key",      BrowserStackKey },
                        //{ "browserstack.local",    "true" },
                        { "browserstack.debug",    "true" },//for screenshots
                        { "browser",              "FireFox" },
                        { "browser_version",      "49.0" },
                        { "os",                   "Windows" },
                        { "os_version",           "10" },
                       // { "resolution",          "2048x1536" },
                        { "build",              "00001" },
                        { "project",            "DSSAutomationFramework" }
                    };

        }

        private void iPhone6NavigatorOnBrowserStack()
        {
            BrowserCapability = new Dictionary<string, object>() {
                        { "browserstack.user",     BrowserStackUserValue },
                        { "browserstack.key",      BrowserStackKey },
                        { "browserstack.local",    "false" },
                        { "browserstack.debug",    "true" },
                        {"platform", "MAC"},
                        { "device",             "iPhone 6S Plus" },
                        { "os",                   "iOS" },
                        { "build",             "Regression_Parallelthreadstrial" },
                        { "project",            "DSSAutomationFramework" },
                        {"name", "Iphone6" }
                       // { "realMobile", "true"}
                    };

        }

        private void ChromeNavigatorOnBrowserStack()
        {
            BrowserCapability = new Dictionary<string, object>() {
                        { "browserstack.user",     BrowserStackUserValue },
                        { "browserstack.key",      BrowserStackKey },
                        { "browserstack.local",    "False" },
                        { "browserstack.debug",    "true" },
                        { "browser",              "Chrome" },
                        { "browser_version",      "54.0" },
                        { "os",                   "Windows" },
                        { "os_version",           "10" },
                         {"quality",        "Compressed" },
                        { "build",              "Regression_DSS" },
                        { "project",            "DSSAutomationFramework" },
                         {"resolution","1920x1080"}

                    };
        }
        private void Chrome1NavigatorOnBrowserStack()
        {
            BrowserCapability = new Dictionary<string, object>() {
                        { "browserstack.user",     BrowserStackUserValue },
                        { "browserstack.key",      BrowserStackKey },
                        //{ "browserstack.local",    "true" },
                        { "browserstack.debug",    "true" },
                        { "browser",              "Chrome" },
                        { "browser_version",      "54.0" },
                        { "os",                   "Windows" },
                        { "os_version",           "8.1" },
                        {"quality",        "Compressed" },
                        { "build",              "Chrome" },
                        { "project",            "DSSAutomationFramework" },
                        {"resolution","1280x800"}
                    };
        }

        private void AndriodGalaxyTabOnBrowserStack()
        {
            BrowserCapability = new Dictionary<string, object>() {
                        { "browserstack.user",     BrowserStackUserValue },
                        { "browserstack.key",      BrowserStackKey },
                        { "browserstack.local",    "true" },
                        { "browserstack.debug",    "true" },
                        { "device",             "Samsung Galaxy S6" },
                        { "os",                   "Android" },
                        { "build",              "Demo_Galaxy" },
                        { "project",            "DSSAutomationFramework" },
                        {"name", "Demo_Home"},
                        {"deviceOrientation","portrait"},
                        {"realMobile","true"}
                    };

        }

        private void iPadMiniNavigatorOnBrowserStack()
        {
            BrowserCapability = new Dictionary<string, object>() {
                        { "browserstack.user",     BrowserStackUserValue },
                        { "browserstack.key",      BrowserStackKey },
                        { "browserstack.local",    "false" },
                        { "browserstack.debug",    "true" },
                        {"platform", "MAC"},
                        { "device",             "iPad Mini" },
                        { "os",                   "iOS" },
                        { "build",              "Mobile_iPhone" },
                        { "project",            "DSSAutomationFramework" },
                        {"name", "Iphone6SExecution"}
                       // { "realMobile",          "true"}
                    };

        }


        #endregion
        #endregion


    }
}
