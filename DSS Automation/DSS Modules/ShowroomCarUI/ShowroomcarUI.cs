﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using System.Linq;

namespace DSS_Automation
{
    [TestClass]
    public class Sprint2_Object
    {
        Common commonobject = new Common();
        public Startpage startpageObject;
        public Sprint1_Object sprint1ObjectMethod;

        public Sprint2_Object(IWebDriver driver)
        {
            PageFactory.InitElements(driver, this);
            startpageObject = new Startpage(driver);
            sprint1ObjectMethod = new Sprint1_Object(driver);
        }

        //TS1
        [FindsBy(How = How.XPath, Using = "//*[@id='5DAD4379-23B9-4973-ABF7-37CBABCEA780']/div/div[1]/div[2]/div/div[1]/h1")]
        public IWebElement carTitleXpath { get; set; }

        [FindsBy(How = How.XPath, Using = "//*[@id='5DAD4379-23B9-4973-ABF7-37CBABCEA780']/div/div[1]/div[2]/div/div[1]/div/span[1]")]
        public IWebElement carUpperSuffixXPath { get; set; }

        [FindsBy(How = How.XPath, Using = "//*[@id='5DAD4379-23B9-4973-ABF7-37CBABCEA780']/div/div[1]/div[2]/div/div[1]/div/span[2]")]
        public IWebElement carLowerSuffixXPath { get; set; }

        [FindsBy(How = How.XPath, Using = "//*[@id='5DAD4379-23B9-4973-ABF7-37CBABCEA780']/div/div[1]/div[2]/div/h3")]
        public IWebElement carSecondaryXPath { get; set; }

        [FindsBy(How = How.XPath, Using = "//*[@id='5DAD4379-23B9-4973-ABF7-37CBABCEA780']/div/div[1]/div[2]/div/div[2]/div[1]/h3")]
        public IWebElement priceXPath { get; set; }

        [FindsBy(How = How.XPath, Using = "//*[@id='5DAD4379-23B9-4973-ABF7-37CBABCEA780']/div/div[1]/div[2]/div/div[2]/div[2]/h4")]
        public IWebElement startingFromXPath { get; set; }

        [FindsBy(How = How.XPath, Using = "//*[@id='5DAD4379-23B9-4973-ABF7-37CBABCEA780']/div/div[2]/div/div")]
        public IWebElement swipeText { get; set; }

        [FindsBy(How = How.XPath, Using = "//*[@id='5DAD4379-23B9-4973-ABF7-37CBABCEA780']/div/div[2]/div/a")]
        public IWebElement swipeIconStart { get; set; }

        //TS2

        [FindsBy(How = How.XPath, Using = "//*[@id='1DD93F01-5757-428F-A41F-5685EFCA33A3']/div/div[3]/div[1]/div/div/div/div[1]/h4")]
        public IWebElement carTitleFLineCDPageXPath { get; set; }

        [FindsBy(How = How.XPath, Using = "//*[@id='1DD93F01-5757-428F-A41F-5685EFCA33A3']/div/div[3]/div[1]/div/div/div/div[1]/h3")]
        public IWebElement carTitleSLineCDPageXPath { get; set; }

        [FindsBy(How = How.XPath, Using = "//*[@id='1DD93F01-5757-428F-A41F-5685EFCA33A3']/div/div[3]/div[1]/div/div/div/div[2]/h2")]
        public IWebElement carDetailsCoverPageTitleXpath { get; set; }

        [FindsBy(How = How.XPath, Using = "//*[@id='1DD93F01-5757-428F-A41F-5685EFCA33A3']/div/div[3]/div[1]/div/div/div/div[3]/div[1]/div/span")]
        public IWebElement keyHighlightTitleXPath { get; set; }

        [FindsBy(How = How.XPath, Using = "//*[@id='1DD93F01-5757-428F-A41F-5685EFCA33A3']/div/div[3]/div[1]/div/div/div/div[3]/div[2]/p[1]")]
        public IWebElement keyHighlightDesc1Xpath { get; set; }

        [FindsBy(How = How.XPath, Using = "//*[@id='1DD93F01-5757-428F-A41F-5685EFCA33A3']/div/div[3]/div[1]/div/div/div/div[3]/div[2]/p[2]")]
        public IWebElement keyHighlightDesc2Xpath { get; set; }

        [FindsBy(How = How.XPath, Using = "//*[@id='1DD93F01-5757-428F-A41F-5685EFCA33A3']/div/div[3]/div[1]/div/div/div/div[3]/div[2]/p[3]")]
        public IWebElement keyHighlightDesc3Xpath { get; set; }

        [FindsBy(How = How.XPath, Using = "//*[@id='1DD93F01-5757-428F-A41F-5685EFCA33A3']/div/div[4]/a")]
        public IWebElement swipeIconCCXPath { get; set; }

        //TS3


        [FindsBy(How = How.XPath, Using = "//*[@id='1DD93F01-5757-428F-A41F-5685EFCA33A3_T']/div/div[1]/div[2]/div[1]/div[2]/div[3]/div/div[1]/h3/span")]
        public IWebElement priceShownCDDXPath { get; set; }

        [FindsBy(How = How.XPath, Using = "//*[@id='1DD93F01-5757-428F-A41F-5685EFCA33A3_T']/div/div[1]/div[2]/div[1]/div[2]/div[3]/div/div[2]/h4")]
        public IWebElement startingAtCDDXPath { get; set; }

        [FindsBy(How = How.XPath, Using = "//*[@id='1DD93F01-5757-428F-A41F-5685EFCA33A3_T']/div/div[1]/div[2]/div[2]/div/div[1]")]
        public IWebElement technicalSpecificationTitleCDDXPath { get; set; }

        [FindsBy(How = How.XPath, Using = "//*[@id='1DD93F01-5757-428F-A41F-5685EFCA33A3_T']/div/div[1]/div[2]/div[2]/div/div[2]")]
        public IWebElement technicalSpecificationCTACDDXPath { get; set; }

        [FindsBy(How = How.XPath, Using = "//div[contains(@class,'showroom-content')]/div/div/div/h4")]
        public IWebElement technicalSpecificationsTitleXpath { get; set; }

        [FindsBy(How = How.XPath, Using = "//div[contains(@class,'showroom-content')]/div/div/a")]
        public IWebElement technicalSpecificationsCloseIconXpath { get; set; }

        [FindsBy(How = How.XPath, Using = "//*[@id='1DD93F01-5757-428F-A41F-5685EFCA33A3_T']/div/div[1]/div[2]/div[1]/div[2]/div[4]/button[1]")]
        public IWebElement testDriveCTACDDXpath { get; set; }

        [FindsBy(How = How.XPath, Using = "//div[contains(@class,'container-fluid dss test-drive-overlay dss-showroom-navigation')]/div/div/div/div[2]/h2")]
        public IWebElement testDriveTitleXpath { get; set; }

        [FindsBy(How = How.XPath, Using = "//div[contains(@class,'container-fluid dss test-drive-overlay dss-showroom-navigation')]/div/div/div/div/div/span")]
        public IWebElement testDriveCloseIconXpath { get; set; }

        [FindsBy(How = How.XPath, Using = "//*[@id='1DD93F01-5757-428F-A41F-5685EFCA33A3_T']/div/div[1]/div[2]/div[1]/div[2]/div[4]/button[2]")]
        public IWebElement shareByEmailCTACDDXpath { get; set; }

        [FindsBy(How = How.XPath, Using = "//div[contains(@class,'container-fluid dss email-overlay-open dss-showroom-navigation')]/div/div/div/div[2]/form/h2")]
        public IWebElement shareByEmailTitleXpath { get; set; }

        [FindsBy(How = How.XPath, Using = "//div[contains(@class,'container-fluid dss email-overlay-open dss-showroom-navigation')]/div/div/div/div/div/span")]
        public IWebElement shareByEmailCloseIconXpath { get; set; }

        [FindsBy(How = How.XPath, Using = "//*[@id='1DD93F01-5757-428F-A41F-5685EFCA33A3_T']/div/div[2]/a")]
        public IWebElement swipeIconCDDXpath { get; set; }

        //Energy declaration Cover page

        [FindsBy(How = How.XPath, Using = "//*[@id='B11A93C9-0A38-44BC-90C2-601E96F51D12']/div/div[3]/div[1]/div/div/h4")]
        public IWebElement energyDecFirstLineXpath { get; set; }

        [FindsBy(How = How.XPath, Using = "//*[@id='B11A93C9-0A38-44BC-90C2-601E96F51D12']/div/div[3]/div[1]/div/div/h3")]
        public IWebElement energyDecSecLineXpath { get; set; }

        [FindsBy(How = How.XPath, Using = "//*[@id='B11A93C9-0A38-44BC-90C2-601E96F51D12']/div/div[3]/div[1]/div/div/h2")]
        public IWebElement energyDecPageTitleXpath { get; set; }

        [FindsBy(How = How.XPath, Using = "//*[@id='B11A93C9-0A38-44BC-90C2-601E96F51D12']/div/div[3]/div[2]/button[1]")]
        public IWebElement energyDecPageTestDriveXpath { get; set; }

        [FindsBy(How = How.XPath, Using = "//*[@id='B11A93C9-0A38-44BC-90C2-601E96F51D12']/div/div[3]/div[2]/button[2]")]
        public IWebElement energyDecPageShareEmailXpath { get; set; }

        [FindsBy(How = How.XPath, Using = "//*[@id='B11A93C9-0A38-44BC-90C2-601E96F51D12']/div/div[4]/a")]
        public IWebElement swipeIconEnergyDecXpath { get; set; }

        //Media cover page
        [FindsBy(How = How.XPath, Using = "//*[@id='9FB2C258-8B99-4A21-A0CF-024A2F17900A']/div/div/div/div[3]/div/div/div/h2")]
        public IWebElement mediaTitleXpath { get; set; }


        [FindsBy(How = How.XPath, Using = "//*[@id='9FB2C258-8B99-4A21-A0CF-024A2F17900A']/div/div/div/div[4]/button[1]/text()")]
        public IWebElement testDriveMediaXpath { get; set; }

        [FindsBy(How = How.XPath, Using = "//*[@id='9FB2C258-8B99-4A21-A0CF-024A2F17900A']/div/div/div/div[4]/button[2]/text()")]
        public IWebElement shareByEmailMediaXpath { get; set; }


        [FindsBy(How = How.XPath, Using = "//*[@id='9FB2C258-8B99-4A21-A0CF-024A2F17900A']/div/div/div/div[5]/a")]
        public IWebElement swipeIconMediaXpath { get; set; }

        //Featured videos page


        [FindsBy(How = How.XPath, Using = "//*[@id='9FB2C258-8B99-4A21-A0CF-024A2F17900A_T']/div/div[2]/h2")]
        public IWebElement featuredVideoTitleXpath { get; set; }


        [FindsBy(How = How.XPath, Using = "//*[@id='9FB2C258-8B99-4A21-A0CF-024A2F17900A_T']/div/div[2]/div[2]")]
        public IWebElement featuredVideoDisclaimerXpath { get; set; }

        [FindsBy(How = How.XPath, Using = "//*[@id='9FB2C258-8B99-4A21-A0CF-024A2F17900A_T']/div/div[2]/div[3]/a")]
        public IWebElement swipeIconFeaturedVideoXpath { get; set; }

        //Gallery page

        [FindsBy(How = How.XPath, Using = "//*[@id='965dd322-2acd-48e6-85d3-58a6a0cb8837']/h2")]
        public IWebElement galleryTitleXpath { get; set; }

        [FindsBy(How = How.XPath, Using = "//*[@id='965dd322-2acd-48e6-85d3-58a6a0cb8837']/p")]
        public IWebElement gallerySubHeaderXPath { get; set; }

        [FindsBy(How = How.XPath, Using = "//*[@id='gallery']/div[3]")]
        public IWebElement galleryDisclaimerXpath { get; set; }

        [FindsBy(How = How.XPath, Using = "//*[@id='gallery']/div[4]/a")]
        public IWebElement swipeIconGalleryXpath { get; set; }


        //Footer page

        [FindsBy(How = How.XPath, Using = "//*[@id='back-top']")]
        public IWebElement backToStartXpath { get; set; }

        [FindsBy(How = How.XPath, Using = "//*[@id='footer-section']/div[2]/button[1]")]
        public IWebElement backToStartTestDriveXpath { get; set; }

        [FindsBy(How = How.XPath, Using = "//*[@id='footer-section']/div[2]/button[2]")]
        public IWebElement backToStartShareByEmailXpath { get; set; }

        [FindsBy(How = How.XPath, Using = "//*[@id='footer-section']/div[3]/p")]
        public IWebElement swipeIconBackToStartXpath { get; set; }

        //variable declaration

        public string previewURL;

        //Calls the preview method which returns the showroom car URL
        //public void PreviewURLCallMethod(IWebDriver driver)
        //{
        //    startpageObject.startpageMethod(driver, "Showroom Admin", "vccbiz\\speriyas", "Selva!^volvo");
        //    previewURL = sprint1ObjectMethod.showroomPathGenerator(driver, carUpperSuffixValue, secondaryTitleEdit);
        //}

        //Launches the showroom car application from the preview URL returned from the Sprint5TS1Method in Sprint1 class
        [TestMethod]
        public void showroomUILaunch(IWebDriver driver)
        {
            driver.Navigate().GoToUrl(previewURL);
            System.Threading.Thread.Sleep(3000);
            string tabName = driver.WindowHandles.Last();
            driver.SwitchTo().Window(tabName);
            System.Threading.Thread.Sleep(5000);
        }

        //Start page verification
        [TestMethod]
        [TestCategory("TS2")]
        public void startPageVerification(IWebDriver driver, string carTitleValue, string carUpperSuffixValue, string carLowerSuffixValue, string secondaryTitleEdit, string priceValue, string startingAtValue, string swipeTextValue)
        {
            commonobject.AssertIsTrueMethod(driver, carTitleXpath, carTitleValue);
            commonobject.AssertIsTrueMethod(driver, carUpperSuffixXPath, carUpperSuffixValue);
            commonobject.AssertIsTrueMethod(driver, carLowerSuffixXPath, carLowerSuffixValue);
            commonobject.AssertIsTrueMethod(driver, carSecondaryXPath, secondaryTitleEdit);
            commonobject.AssertIsTrueMethod(driver, priceXPath, priceValue);
            commonobject.AssertIsTrueMethod(driver, startingFromXPath, startingAtValue);
            commonobject.AssertIsTrueMethod(driver, swipeText, swipeTextValue);
            swipeIconStart.Click();
        }

        //Car details cover page verification
        [TestMethod]
        [TestCategory("TS2")]
        public void carDetailsCoverPageVerification(IWebDriver driver, string carTitleValue,string carUpperSuffixValue,string carLowerSuffixValue, string secondaryTitleEdit, string keyHighlightsTitleValue, string keyhighlightsdescValue)
        {
            showroomUILaunch(driver);
            swipeIconStart.Click();
            string CDFLineValue = carTitleValue + " " + carUpperSuffixValue + " " + carLowerSuffixValue;
            commonobject.AssertIsTrueMethod(driver, carTitleFLineCDPageXPath, CDFLineValue);
            commonobject.AssertIsTrueMethod(driver, carTitleSLineCDPageXPath, secondaryTitleEdit);
            commonobject.AssertIsTrueMethod(driver, keyHighlightTitleXPath, keyHighlightsTitleValue);
            commonobject.AssertIsTrueMethod(driver, keyHighlightDesc1Xpath, keyhighlightsdescValue);
            commonobject.AssertIsTrueMethod(driver, keyHighlightDesc2Xpath, "");
            commonobject.AssertIsTrueMethod(driver, keyHighlightDesc3Xpath, "");
            swipeIconCCXPath.Click();
        }

        //Car details details page verification
        [TestMethod]
        [TestCategory("TS2")]
        public void carDetailsdetailPageVerification(IWebDriver driver, string priceValue, string startingAtValue)
        {
            showroomUILaunch(driver);
            swipeIconStart.Click();
            System.Threading.Thread.Sleep(5000);
            swipeIconCCXPath.Click();
            commonobject.AssertIsTrueMethod(driver, priceShownCDDXPath, priceValue);
            commonobject.AssertIsTrueMethod(driver, startingAtCDDXPath, startingAtValue);
        }

        //Technical Specifications overlay verification
        [TestMethod]
        [TestCategory("TS2")]
        public void techSpecificationOverlayVerification(IWebDriver driver, string technicalSpecificationPageTitleText, string technicalSpecificationCTAText, string technicalSpecificationCDDHeaderText)
        {
            showroomUILaunch(driver);
            swipeIconStart.Click();
            System.Threading.Thread.Sleep(5000);
            swipeIconCCXPath.Click();
            commonobject.AssertIsTrueMethod(driver, technicalSpecificationTitleCDDXPath, technicalSpecificationPageTitleText);
            commonobject.AssertIsTrueMethod(driver, technicalSpecificationCTACDDXPath, technicalSpecificationCTAText);
            technicalSpecificationCTACDDXPath.Click();
            commonobject.AssertIsTrueMethod(driver, technicalSpecificationsTitleXpath, technicalSpecificationCDDHeaderText);
            technicalSpecificationsCloseIconXpath.Click();

        }

        //Equipment & features overlay verification
        [TestMethod]
        [TestCategory("TS2")]
        public void EquipmentOverlayVerification(IWebDriver driver)
        {
            showroomUILaunch(driver);
            swipeIconStart.Click();
            swipeIconCCXPath.Click();


        }

        //Test Drive CTA verification in Car details details page.
        [TestMethod]
        [TestCategory("TS2")]
        public void testDriveOverlay(IWebDriver driver, string testDriveCTAText, string testDrivePageTitleText)
        {
            showroomUILaunch(driver);
            swipeIconStart.Click();
            swipeIconCCXPath.Click();
            commonobject.AssertIsTrueMethod(driver, testDriveCTACDDXpath, testDriveCTAText);
            testDriveCTACDDXpath.Click();
            commonobject.AssertIsTrueMethod(driver, testDriveTitleXpath, testDrivePageTitleText);
            testDriveCloseIconXpath.Click();
        }

        //Share by email verification in car details - details page
        [TestMethod]
        [TestCategory("TS2")]
        public void shareByEmailOverlay(IWebDriver driver, string shareByEmailCTAText, string shareByEmailPageTitleText)
        {
            showroomUILaunch(driver);
            swipeIconStart.Click();
            swipeIconCCXPath.Click();
            commonobject.AssertIsTrueMethod(driver, shareByEmailCTACDDXpath, shareByEmailCTAText);
            shareByEmailCTACDDXpath.Click();
            commonobject.AssertIsTrueMethod(driver, shareByEmailTitleXpath, shareByEmailPageTitleText);
            shareByEmailCloseIconXpath.Click();
            swipeIconCDDXpath.Click();
        }

        //Energy declaration cover page verification
        [TestMethod]
        public void EnergydeclarationMethod(IWebDriver driver, string carTitleValue, string carUpperSuffixValue, string carLowerSuffixValue, string secondaryTitleEdit, string energyDeclarationTitleText)
        {
            showroomUILaunch(driver);
            swipeIconStart.Click();
            swipeIconCCXPath.Click();
            swipeIconCDDXpath.Click();
            string EnergydeclarationFirstLineText = carTitleValue + " " + carUpperSuffixValue + " " + carLowerSuffixValue;
            commonobject.AssertIsTrueMethod(driver, energyDecFirstLineXpath, EnergydeclarationFirstLineText);
            commonobject.AssertIsTrueMethod(driver, energyDecSecLineXpath, secondaryTitleEdit);
            commonobject.AssertIsTrueMethod(driver, energyDecPageTitleXpath, energyDeclarationTitleText);
            swipeIconEnergyDecXpath.Click();
        }

        //Media Cover page verification
        [TestMethod]
        public void MediaCoverPageMethod(IWebDriver driver, string mediaTitleText, string testDriveCTAText, string shareByEmailCTAText)
        {
            showroomUILaunch(driver);
            swipeIconStart.Click();
            swipeIconCCXPath.Click();
            swipeIconCDDXpath.Click();
            swipeIconEnergyDecXpath.Click();
            commonobject.AssertIsTrueMethod(driver, mediaTitleXpath, mediaTitleText);
            commonobject.AssertIsTrueMethod(driver, testDriveMediaXpath, testDriveCTAText);
            commonobject.AssertIsTrueMethod(driver, shareByEmailMediaXpath, shareByEmailCTAText);
            swipeIconMediaXpath.Click();
        }

        //Featured Videos page verification
        [TestMethod]
        public void FeaturedVideoPageMethod(IWebDriver driver, string featuredVideosTitleText, string featuedVideoDisclaimertext)
        {
            showroomUILaunch(driver);
            swipeIconStart.Click();
            swipeIconCCXPath.Click();
            swipeIconCDDXpath.Click();
            swipeIconEnergyDecXpath.Click();
            swipeIconMediaXpath.Click();
            commonobject.AssertIsTrueMethod(driver, featuredVideoTitleXpath, featuredVideosTitleText);
            commonobject.AssertIsTrueMethod(driver, featuredVideoDisclaimerXpath, featuedVideoDisclaimertext);
            swipeIconFeaturedVideoXpath.Click();
        }

        //Gallery Page verification
        [TestMethod]
        public void GalleryPageMethod(IWebDriver driver, string galleryTitleText, string gallerySubHeaderText)
        {
            showroomUILaunch(driver);
            swipeIconStart.Click();
            swipeIconCCXPath.Click();
            swipeIconCDDXpath.Click();
            swipeIconEnergyDecXpath.Click();
            swipeIconMediaXpath.Click();
            swipeIconFeaturedVideoXpath.Click();
            commonobject.AssertIsTrueMethod(driver, featuredVideoTitleXpath, galleryTitleText);
            commonobject.AssertIsTrueMethod(driver, featuredVideoDisclaimerXpath, gallerySubHeaderText);
            commonobject.AssertIsTrueMethod(driver, featuredVideoDisclaimerXpath, gallerySubHeaderText);
            swipeIconGalleryXpath.Click();
        }

        //Footer page verification
        [TestMethod]
        public void footerpageMethod(IWebDriver driver, string backToStartCTAText, string testDriveCTAText, string shareByEmailCTAText)
        {
            showroomUILaunch(driver);
            swipeIconStart.Click();
            swipeIconCCXPath.Click();
            swipeIconCDDXpath.Click();
            swipeIconEnergyDecXpath.Click();
            swipeIconMediaXpath.Click();
            swipeIconFeaturedVideoXpath.Click();
            swipeIconGalleryXpath.Click();
            commonobject.AssertIsTrueMethod(driver, backToStartXpath, backToStartCTAText);
            commonobject.AssertIsTrueMethod(driver, backToStartTestDriveXpath, testDriveCTAText);
            commonobject.AssertIsTrueMethod(driver, backToStartShareByEmailXpath, shareByEmailCTAText);
            backToStartXpath.Click();
        }
    }
}
