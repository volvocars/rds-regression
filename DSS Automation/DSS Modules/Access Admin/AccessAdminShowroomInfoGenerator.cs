﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;

namespace DSS_Automation.DSS_Modules.Access_Admin
{
    [TestClass]
    public class AccessAdminShowroomInfoGenerator
    {
        public string supportedBrowser { get; set; }
        public string sessionName { get; set; }
        public Startpage start_object;
        public Common co_object;


        
        [TestInitialize]

        public void initiateTest()
        {
            /*
            supportedBrowser = Convert.ToString(TestContext.DataRow["Browser Name"]);
            sessionName = Convert.ToString(TestContext.DataRow["Session Name"]);
            string userName = Convert.ToString(TestContext.DataRow["Access Admin Username"]);
            string passWord = Convert.ToString(TestContext.DataRow["Access Admin Password"]);
 */
            supportedBrowser = "Chrome_Browser";
            sessionName = "Access Admin showroom Info Generator";
            //Browserstack S2 object creation
            SetUpBrowserStackDriver();
            start_object = new Startpage(driver);
            co_object = new Common();
            start_object.startpageMethod(driver, "Access Admin", @"vccbiz\speriyas", "Selva()volvo");
        }

        [TestMethod]
        public void showroomInfoGenerator()
        {
            int showroomCount = driver.FindElements(By.XPath("//div[@class='paginationWrapper custom-row']//div[@class='row list-row']")).Count;
            int pageCount = showroomCount % 10;
            driver.FindElement(By.XPath("//div[contains(@class,'paginationWrapper')]//div[@class='row list-row'][1]//a[contains(text(),'Edit')]")).Click();
            co_object.waitForElementUntilVisible(@"//button[contains(text(),'Cancel')]", driver, 30);
            int userCount=driver.FindElements(By.XPath("//div[@class='userName manageCheckWrapper']//input")).Count;
            string result;
            
            if (userCount > 0)
            {
                string[] userTempArray = new string[userCount];
                for (int count = 1; count <= userCount; count++)
                {
                    userTempArray[count - 1] = driver.FindElement(By.XPath("//div[@class='userName manageCheckWrapper']//input["+count+"]")).GetAttribute("value");
                }

                result = String.Join(",", userTempArray);
            }
            driver.FindElement(By.XPath("//button[contains(text(),'Cancel')]")).Click();
            //div[@class='paginationWrapper custom-row']//div[((@class='row list-row') | @style='display: block;') and not(@style='display: none;')]
        }





        #region Browserstack
        // public string env = ConfigurationManager.AppSettings["Environment"];

        //Login for Sitecore for Authoring
        public const string usernameTextBox = ".//*[@id='UserName']";
        public const string passwordTextBox = ".//*[@id='Password']";
        public const string loginButton = ".//*[@id='login']/input";
        //BrowserStack credencials

        #region BrowserStack Configurations

        /* public string GetTestMethodName(string name)
        {
            return TestContext.TestName;

        }*/
        public string BrowserStackUserValue { get; set; }
        public string BrowserStackKey { get; set; }
        public string methodName;
        //  public TestContext TestContext { get; set; }
        //Web or Mobile version to Launch
        //public string supportedBrowser = ConfigurationManager.AppSettings["TargetBrowser"];
        public Dictionary<string, object> BrowserCapability { get; set; }

        public IWebDriver driver;

        //public Driver()
        //{
        //    BrowserStackUser = ConfigurationManager.AppSettings["BrowserStackUser"];
        //    BrowserStackKey = ConfigurationManager.AppSettings["BroserStackKey"];
        //    //Initiale all the properties of Browser that need to be launched
        //    InitialiseBrowser(supportedBrowser);
        //    //Initialise and declare desired capability
        //    DesiredCapabilities capability = new DesiredCapabilities(BrowserCapability);


        //    //Global driver to launch Browser stack with username and Accesskey
        //    driver = new RemoteWebDriver(
        //   new Uri("http://hub-cloud.browserstack.com/wd/hub/"), capability
        // );

        //}

        public void SetUpBrowserStackDriver()
        {
            BrowserStackUserValue = ConfigurationManager.AppSettings["BrowserStackUser"];
            BrowserStackKey = ConfigurationManager.AppSettings["BroserStackKey"];
            //Initiale all the properties of Browser that need to be launched
            InitialiseBrowser(supportedBrowser);
            //Initialise and declare desired capability
            DesiredCapabilities capability = new DesiredCapabilities(BrowserCapability);


            //Global driver to launch Browser stack with username and Accesskey
            driver = new RemoteWebDriver(
           new Uri("http://hub-cloud.browserstack.com/wd/hub/"), capability
         );

        }

        public void InitialiseBrowser(string supportedBrowser)
        {
            //BrowserStackUserValue = ConfigurationManager.AppSettings["BrowserStackUser"];
            //BrowserStackKey = ConfigurationManager.AppSettings["BroserStackKey"];
            //DesiredCapabilities capability = new DesiredCapabilities(BrowserCapability);
            //driver = new RemoteWebDriver(
            //    new Uri("http://hub-cloud.browserstack.com/wd/hub/"), capability
            //    );
            switch (supportedBrowser)
            {
                case "Chrome_Browser":
                    //Tests.HomePageTest.GetTestMethodName();
                    ChromeNavigatorOnBrowserStack();
                    break;

                case "Firefox_Browser":
                    FirefoxNavigator();
                    break;

                case "iPhone6":
                    iPhone6NavigatorOnBrowserStack();
                    break;
                case "iPadMini":
                    iPadMiniNavigatorOnBrowserStack();
                    break;
                case "Galaxy":
                    AndriodGalaxyTabOnBrowserStack();
                    break;

                default:
                    FirefoxNavigator();
                    break;
            }
        }

        private void FirefoxNavigator()
        {
            BrowserCapability = new Dictionary<string, object>() {
                        { "browserstack.user",     BrowserStackUserValue },
                        { "browserstack.key",      BrowserStackKey },
                        //{ "browserstack.local",    "true" },
                        { "browserstack.debug",    "true" },//for screenshots
                        { "browser",              "FireFox" },
                        { "browser_version",      "49.0" },
                        { "os",                   "Windows" },
                        { "os_version",           "10" },
                       // { "resolution",          "2048x1536" },
                        { "build",              "00001" },
                        { "project",            "DSSAutomationFramework" }
                    };

        }

        private void iPhone6NavigatorOnBrowserStack()
        {
            BrowserCapability = new Dictionary<string, object>() {
                        { "browserstack.user",     BrowserStackUserValue },
                        { "browserstack.key",      BrowserStackKey },
                        { "browserstack.local",    "false" },
                        { "browserstack.debug",    "true" },
                        {"platform", "MAC"},
                        { "device",             "iPhone 6S Plus" },
                        { "os",                   "iOS" },
                        { "build",             "Regression_Parallelthreadstrial" },
                        { "project",            "DSSAutomationFramework" },
                        {"name", "Iphone6" }
                       // { "realMobile", "true"}
                    };

        }

        private void ChromeNavigatorOnBrowserStack()
        {
            BrowserCapability = new Dictionary<string, object>() {
                        { "browserstack.user",     BrowserStackUserValue },
                        { "browserstack.key",      BrowserStackKey },
                        { "browserstack.local",    "False" },
                        { "browserstack.debug",    "true" },
                        { "browser",              "Chrome" },
                        { "browser_version",      "54.0" },
                        { "os",                   "Windows" },
                        { "os_version",           "10" },
                        {"quality",        "Compressed" },
                        { "build",              "Regression_DSS" },
                        { "project",            "DSSAutomationFramework" },
                        {"resolution","1920x1080"},
                        {"name",sessionName }

                    };
        }
        private void Chrome1NavigatorOnBrowserStack()
        {
            BrowserCapability = new Dictionary<string, object>() {
                        { "browserstack.user",     BrowserStackUserValue },
                        { "browserstack.key",      BrowserStackKey },
                        //{ "browserstack.local",    "true" },
                        { "browserstack.debug",    "true" },
                        { "browser",              "Chrome" },
                        { "browser_version",      "54.0" },
                        { "os",                   "Windows" },
                        { "os_version",           "8.1" },
                        {"quality",        "Compressed" },
                        { "build",              "Chrome" },
                        { "project",            "DSSAutomationFramework" },
                        {"resolution","1280x800"}
                    };
        }

        private void AndriodGalaxyTabOnBrowserStack()
        {
            BrowserCapability = new Dictionary<string, object>() {
                        { "browserstack.user",     BrowserStackUserValue },
                        { "browserstack.key",      BrowserStackKey },
                        { "browserstack.local",    "true" },
                        { "browserstack.debug",    "true" },
                        { "device",             "Samsung Galaxy S6" },
                        { "os",                   "Android" },
                        { "build",              "Demo_Galaxy" },
                        { "project",            "DSSAutomationFramework" },
                        {"name", "Demo_Home"},
                        {"deviceOrientation","portrait"},
                        {"realMobile","true"}
                    };

        }

        private void iPadMiniNavigatorOnBrowserStack()
        {
            BrowserCapability = new Dictionary<string, object>() {
                        { "browserstack.user",     BrowserStackUserValue },
                        { "browserstack.key",      BrowserStackKey },
                        { "browserstack.local",    "false" },
                        { "browserstack.debug",    "true" },
                        {"platform", "MAC"},
                        { "device",             "iPad Mini" },
                        { "os",                   "iOS" },
                        { "build",              "Mobile_iPhone" },
                        { "project",            "DSSAutomationFramework" },
                        {"name", "Iphone6SExecution"}
                       // { "realMobile",          "true"}
                    };

        }


        #endregion
        #endregion
    }
}
