﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;
using System.Linq;

namespace DSS_Automation
{
    [TestClass]
    public class AccessAdmin
    {

        Common commonobject = new Common();
        public AccessAdmin(IWebDriver driver)
        {
            PageFactory.InitElements(driver, this);

        }

        //Access Admin page objects
        [FindsBy(How = How.XPath, Using = "//button[contains(@class,'button-small button-opaque js-createNew')]")]
        public IWebElement createNewShowroom { get; set; }


        //Create New showroom page objects
        [FindsBy(How = How.XPath, Using = "//*[@id='ShowroomName']")]
        public IWebElement showroomName { get; set; }

        //*[@id="volvo-dss"]/div[2]/div/div[5]/div[1]/button
        //[FindsBy(How = How.XPath, Using = "//select[contains(@class,'hangeshowroom dd-close')]")]
        //public IWebElement showroomTypeSelection { get; set; }

        [FindsBy(How = How.XPath, Using = "//*[@id='SelectedShowroomType']")]
        public IWebElement showroomTypeSelection { get; set; }

        [FindsBy(How = How.XPath, Using = "//*[@id='DealerAddress']")]
        public IWebElement dealerAddress { get; set; }

        [FindsBy(How = How.XPath, Using = "//*[@id='DealerPhoneNumber']")]
        public IWebElement dealerPhoneNumber { get; set; }

        [FindsBy(How = How.XPath, Using = "//*[@id='DealerWebsite']")]
        public IWebElement dealerWebsite { get; set; }

        [FindsBy(How = How.XPath, Using = "//button[contains(@data-target,'#addMarketModal')]")]
        public IWebElement addMarketOverlay { get; set; }

        [FindsBy(How = How.XPath, Using = "//div[contains(@class,'row custom-row marketCheck')]")]
        public IWebElement addMarketList { get; set; }

        [FindsBy(How = How.XPath, Using = "//button[contains(@class,'button-small button-opaque addMarket')]")]
        public IWebElement addMarketOKButton { get; set; }

        [FindsBy(How = How.XPath, Using = "//button[contains(@data-target,'#addLanguage')]")]
        public IWebElement addLanguageOverlay { get; set; }

        [FindsBy(How = How.XPath, Using = "//div[contains(@class,'row custom-row langCheck')]")]
        public IWebElement addLanguageList { get; set; }

        [FindsBy(How = How.XPath, Using = "//button[contains(@class,'button-small button-opaque addLanguage')]")]
        public IWebElement addLanguageOKButton { get; set; }

        [FindsBy(How = How.XPath, Using = "//*[@id='csidInput']")]
        public IWebElement addCDSID { get; set; }

        [FindsBy(How = How.XPath, Using = "//button[contains(@class,'button-small button-opaque addBtn')]")]
        public IWebElement addCDSIDButton { get; set; }


        [FindsBy(How = How.XPath, Using = "//button[contains(@class,'button-small button-opaque js-dss-save')]")]
        public IWebElement saveShowroomButton { get; set; }

        //variable declaration

        string publishShowroomXPath;

        //Method definition

        //Create new showroom button
        public void addNewShowroom(IWebDriver driver, string showroomTypeValue, string showroomNameValue, string showroomAddressValue, string showroomPhoneNumberValue, string websiteValue, string CDSID1Value, string CDSID2Value)
        {
            commonobject.MovetoElementMethodFromElement(driver, createNewShowroom);
            //createNewShowroom.Click();
            System.Threading.Thread.Sleep(3000);
            showroomName.SendKeys(showroomNameValue);
            SelectElement ShowroomTypeDDL = new SelectElement(showroomTypeSelection);
            ShowroomTypeDDL.SelectByText(showroomTypeValue);
            dealerAddress.SendKeys(showroomAddressValue);
            dealerPhoneNumber.SendKeys(showroomPhoneNumberValue);
            dealerWebsite.SendKeys(websiteValue);
            addMarketOverlay.Click();
            var marketCount = driver.FindElements(By.XPath("//*[contains(@class,'row custom-row marketCheck')]/div")).Count();
            for (var i = 1; i < marketCount - 2; i++)
            {
                driver.FindElement(By.XPath("//*[@id='addMarketModal']/div/div/div[2]/div[2]/div[" + i + "]/input")).Click();
            }
            addMarketOKButton.Click();
            addLanguageOverlay.Click();
            var languageCount = driver.FindElements(By.XPath("//*[contains(@class,'row custom-row langCheck')]/div")).Count();
            for (var j = 1; j < languageCount - 2; j++)
            {
                driver.FindElement(By.XPath("//*[@id='addLanguage']/div/div/div[2]/div/div[" + j + "]/input")).Click();
            }
            addLanguageOKButton.Click();
            addCDSID.SendKeys(CDSID1Value);
            addCDSIDButton.Click();
            addCDSID.SendKeys(CDSID2Value);
            addCDSIDButton.Click();
            saveShowroomButton.Click();
            System.Threading.Thread.Sleep(3000);
        }

        public void publishShowroom(IWebDriver driver, string showroomNameValue)
        {
            try
            {
                publishShowroomXPath = "//*[span[contains(text(),'" + showroomNameValue + "')]]/parent::node()/following-sibling::div/p/a[contains(text(),'Publish')]";
                driver.FindElement(By.XPath(publishShowroomXPath)).Click();
            }
            catch (ElementNotVisibleException)
            {
                var pageCount = driver.FindElements(By.XPath("//*[@id='page-nav']/ul/li")).Count();
                var nxtCount = pageCount - 1;
                for (var loopCount = 1; loopCount < nxtCount; loopCount++)
                {
                    try
                    {
                        driver.FindElement(By.XPath("//*[@id='page-nav']/ul/li/following-sibling::li/a[contains(text(),'Next')]")).Click();
                        string publishShowroomXPath = "//*[span[contains(text(),'" + showroomNameValue + "')]]/parent::node()/following-sibling::div/p/a[contains(text(),'Publish')]";
                        driver.FindElement(By.XPath(publishShowroomXPath)).Click();
                    }
                    catch (ElementNotVisibleException)
                    {
                        continue;
                    }
                    break;
                }

            }

        }
        //Delete the showroom in access admin
        public void deleteShowroom(IWebDriver driver, string showroomNameValue)
        {
            try
            {
                string deleteShowroomXpath = "//*[span[contains(text(),'" + showroomNameValue + "')]]/parent::node()/following-sibling::div/p/a/img";
                driver.FindElement(By.XPath(deleteShowroomXpath)).Click();
            }
            catch (ElementNotVisibleException)
            {
                var pageCount = driver.FindElements(By.XPath("//*[@id='page-nav']/ul/li")).Count();
                var nxtCount = pageCount - 2;
                for (var loopCount = 1; loopCount < nxtCount; loopCount++)
                {
                    try
                    {
                        driver.FindElement(By.XPath("//*[@id='page-nav']/ul/li/following-sibling::li/a[contains(text(),'Next')]")).Click();
                        string deleteShowroomXPath = "//*[span[contains(text(),'" + showroomNameValue + "')]]/parent::node()/following-sibling::div/p/a/img";
                        driver.FindElement(By.XPath(deleteShowroomXPath)).Click();
                    }
                    catch (ElementNotVisibleException)
                    {
                        continue;
                    }
                    break;
                }

            }

        }
    }
}
